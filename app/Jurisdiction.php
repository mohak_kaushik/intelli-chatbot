<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurisdiction extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function resources() 
    {
        return $this->belongsToMany('App\Resource', 'jurisdiction_resource', 'jurisdiction_id', 'resource_id');
    }
}
