@extends('layouts.app')
@section('title', 'Resource Category')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Resource Category</h4>
        <a class="titlebtn" href="javascript:void(0);" data-toggle="modal" data-target="#addCategoryModal"><i class="fas fa-plus"></i> Add Category </a>
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="mb-4 float-left filters-input">
        <input type="text" name="search-category" id="search-category" placeholder="Search by name" onchange="filter()" value="{{app('request')->input('category')}}">
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Category</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td>
                    @if($category->status == '1')
                        <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                    @else 
                        <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                    @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ route('resource-category.edit',$category->id) }}" class="category-edit" data-toggle="modal" data-target="#editCategoryModal"><i class="fas fa-edit"></i></a>
                        <!-- <a href="{{ route('resource-category.destroy',$category->id) }}" class="category-delete"><i class="fas fa-trash-alt"></i></a> -->
                    </td>
                </tr>
                @empty
                    <tr><td colspan="3" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$categories->appends(['category' => app('request')->input('category'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>

<!-- Add Category Modal -->
<div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="addCategoryModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>'resource-category.store','method'=>'POST','id'=>'add-category-form']) !!}
                    <div class="formdesign">
                        <div class="row">
                            <div class="w-100">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="add_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status </label>
                                        <select name="add_status" class="form-control">
                                        @foreach(Config::get('intelli.status') as $sKey => $sVal)
                                            <option value="{{ $sKey }}" @if($sKey=='1') {{ 'selected' }} @endif>{{ $sVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Edit Category Modal -->
<div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editCategoryModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>['resource-category.update', ''],'method'=>'PUT','id'=>'edit-category-form']) !!}
                    <div class="formdesign">
                        <div class="row">
                            <div class="w-100">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="edit_name" id="edit_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status </label>
                                        <select name="edit_status" id="edit_status" class="form-control">
                                        @foreach(Config::get('intelli.status') as $sKey => $sVal)
                                            <option value="{{ $sKey }}">{{ $sVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        //Category add
        $('#add-category-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action');
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                    error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        //Category edit
        var cid = null;
        $('.category-edit').on('click',function(e){
            e.preventDefault();
            let url = $(this).attr("href");
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    cid = response.id;
                    $('#edit_name').val(response.name)
                    $('#edit_status').val(response.status)
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        })

        //Category update
        $('#edit-category-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action')+'/'+cid;
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        //User delete
        // $('.user-delete').on('click', function (e) {
        //     var url = $(this).attr("href");
        //     var result = confirm('Are you sure you want to delete the user?'); 
        //     if (result == true) {
        //         $.ajax({
        //             url: url,
        //             type: 'POST',
        //             data: {
        //                 '_method': 'delete',
        //                 '_token': "{{csrf_token()}}"
        //             },
        //             success: function (data) {
        //                 window.location.reload();
        //             },
        //         });
        //     }
        //     e.preventDefault()
        // });
    });

    function filter() {
        var cat = $('#search-category').val();
        var status = $('#search-status').val();
        var url = APP_URL+"/{{Request::segment(1)}}?category="+cat+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection