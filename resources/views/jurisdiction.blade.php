@extends('layouts.app')
@section('title', 'Jurisdiction')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Jurisdictions</h4>
        @can('jurisdiction-create')
        <button type="button" class="mapbtn"><i class="fas fa-arrow-down"></i> Add Jurisdiction </button>
        @endcan
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="mb-4  filters-input jurisdictionFilter">
        <input type="text" name="search-jurisdiction" id="search-jurisdiction" placeholder="Search by name" onchange="filter()" value="{{app('request')->input('jurisdiction')}}">
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="jurisdictions_map">
        <div id="mapfilter" class="p-3">
            <input id="pac-input" class="controls" type="text" placeholder="Search Here">
            <button type="button" class="btnstyle" id="drawmap" onclick="deleteMarkers();">Reset</button>
        </div>
        <div id="map"></div>
        <div class="formdesign px-3 py-4" style="background: #f7f7f7;box-shadow: inset 0px -1px 0px 1px #e6e6e6;">
            {!! Form::open(['route'=>'jurisdictions.store','method'=>'POST','id'=>'add-jurisdiction-form']) !!}
                <input type="hidden" name="minlatitude" id="minlatitude">
                <input type="hidden" name="maxlatitude" id="maxlatitude">
                <input type="hidden" name="minlongitude" id="minlongitude">
                <input type="hidden" name="maxlongitude" id="maxlongitude">
                <div class="row">
                    <div class="col-md-12">
                        <label><strong>Jurisdiction ID / Jurisdiction Name</strong></label>
                        <div class="form-group">
                            <input type="text" name="nameid" class="form-control" id="jurisdictionname">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label><strong>Point One</strong></label>
                        <div class="row">
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="pointonelat" class="form-control" id="pointlat_1" readonly>
                                </div>
                            </div>
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="pointonelng" class="form-control" id="pointlng_1" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label><strong>Point Two</strong></label>
                        <div class="row">
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="pointtwolat" class="form-control" id="pointlat_2" readonly>
                                </div>
                            </div>
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="pointtwolng" class="form-control" id="pointlng_2" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label><strong>Point Three</strong></label>
                        <div class="row">
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="pointthreelat" class="form-control" id="pointlat_3"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="pointthreelng" class="form-control" id="pointlng_3"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label><strong>Point Four</strong></label>
                        <div class="row">
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="pointfourlat" class="form-control" id="pointlat_4"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-6 latlngpoints">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="pointfourlng" class="form-control" id="pointlng_4"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 text-left">
                        <button type="submit" class="btnstyle">Submit</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Jurisdiction</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Last Modified</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($jurisdictions as $jurisdiction)
                <tr>
                    <td>{{ $jurisdiction->name }}</td>
                    <td>{{ $jurisdiction->user->name }}</td>
                    <td>
                        @if($jurisdiction->last_updated_by)
                            {{ \Carbon\Carbon::parse($jurisdiction->updated_at)->format('d M Y')}}
                        @else
                            {{ \Carbon\Carbon::parse($jurisdiction->created_at)->format('d M Y')}}
                        @endif
                    </td>
                    <td>
                    @if($jurisdiction->status == '1')
                        <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                    @else 
                        <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                    @endif
                    </td>
                    <td class="text-center">
                        @can('jurisdiction-edit')
                        <a href="{{ route('jurisdictions.edit', $jurisdiction->id) }}" class="jurisdiction-edit"><i class="fas fa-edit"></i></a>
                        @endcan
                        @can('jurisdiction-delete')
                        <a href="{{ route('jurisdictions.destroy',$jurisdiction->id) }}" class="jurisdiction-delete"><i class="fas fa-trash-alt"></i></a>
                        @endcan
                    </td>
                </tr>
                @empty
                    <tr><td colspan="6" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$jurisdictions->appends(['jurisdiction' => app('request')->input('jurisdiction'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>
@endsection

@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI2X826A0AJ5dGzXkeQQDRkzJV5-5JHE4&libraries=places&callback=initMap"></script>
<script src="{{ asset('js/main/jurisdiction.map.js') }}"></script>
<script>
    $(document).ready(function () {

        $('#add-jurisdiction-form').submit(function(e){
            e.preventDefault();
            var formData = $('#add-jurisdiction-form').serialize();
            let url = $(this).attr('action');
            let method = $(this).attr('method');
            $('.error').remove()
            
            $.ajax({
                url: url,
                type: method,
                data: formData,
                success: function (response) {
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "']").after("<small class='error form-text text-danger'>" + value[0] + "</small>")                      
                    })
                },
            });
        });

        $('.jurisdiction-delete').on('click', function (e) {
            var url = $(this).attr("href");
            var result = confirm('Are you sure you want to delete the jurisdiction?'); 
            if (result == true) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        '_method': 'delete',
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        window.location.reload();
                    },
                });
            }
            e.preventDefault()
        });

    }); // Ready Close Here

    function filter() {
        var jur = $('#search-jurisdiction').val();
        var status = $('#search-status').val();
        var url = APP_URL+"/{{Request::segment(1)}}?jurisdiction="+jur+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection
