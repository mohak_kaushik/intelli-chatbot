@extends('layouts.app')
@section('title', 'Apps')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Apps</h4>
        <!-- <button type="button"><i class="fas fa-plus"></i> New App </button> -->
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="mb-4 float-left filters-input">
        <input type="text" name="search-app" id="search-app" placeholder="Search by name" onchange="filter()" value="{{app('request')->input('app')}}">
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">App Name</th>
                    <th scope="col">Package Name</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Last Modified</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($apps as $app)
                <tr>
                    <td>
                        <div class="thumbnail">
                        @if($app->logo)
                            <span class="img"><img src="{{ url('upload/app-logo') .'/'. $app->logo }}" alt="{{ $app->name }}"></span>
                        @else
                            <span class="img"><img src="{{ url('images/chooseprofile.png') }}" alt="{{ $app->name }}"></span>
                        @endif
                            <p>{{ $app->name }}</p>
                        </div>
                    </td>
                    <td>{{ $app->package_name }}</td>
                    <td>{{ $app->user->name }}</td>
                    <td>
                        @if($app->last_updated_by)
                            {{ \Carbon\Carbon::parse($app->updated_at)->format('d M Y')}}
                        @else
                            {{ \Carbon\Carbon::parse($app->created_at)->format('d M Y')}}
                        @endif
                    </td>
                    <td>
                    @if($app->status == '1')
                        <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                    @else 
                        <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                    @endif
                    </td>
                    <td class="text-center">
                        @can('app-edit')
                        <a href="{{ route('apps.edit',$app->id) }}" class="app-edit" data-toggle="modal" data-target="#editAppData"><i class="fas fa-edit"></i></a>
                        @endcan
                        @can('app-delete')
                        <a href="{{ route('apps.destroy',$app->id) }}" class="app-delete"><i class="fas fa-trash-alt"></i></a>
                        @endcan
                    </td>
                </tr>
                @empty
                    <tr><td colspan="6" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$apps->appends(['app' => app('request')->input('app'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editAppData" tabindex="-1" role="dialog" aria-labelledby="editAppData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit App</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>['apps.update', ''],'method'=>'PUT','id'=>'edit-app-form','files'=>true]) !!}
                    <div class="formdesign">
                        <div class="row logo-field">
                            <div class="col-12">
                                <div class="chooseprofile text-center mb-3">
                                    <div class="profile_pic">
                                        <img id="edit_app_logo" src="{{url('images/chooseprofile.png')}}" alt="" />
                                    </div>
                                    <div class="upload-btn-wrapper">
                                        <button class=" btn btn-primary">Upload a file</button>
                                        <input type="file" name="edit_app_logo" onchange="readURL(this, 'edit_app_logo');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Name <span style="color:red;">*</span></label>
                                    <input type="text" name="edit_app_name" id="edit_app_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Package Name <span style="color:red;">*</span></label>
                                    <input type="text" name="edit_app_package_name" id="edit_app_package_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Operator Name <span style="color:red;">*</span></label>
                                    <input type="text" name="edit_app_operator_name" id="edit_app_operator_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Operator Email <span style="color:red;">*</span></label>
                                    <input type="text" name="edit_app_operator_email" id="edit_app_operator_email" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Category <span style="color:red;">*</span></label>
                                    {!! Form::select('edit_app_category', $categories, null, array('id' => 'edit_app_category', 'class' => 'form-control', 'placeholder' => 'Select Category')) !!}
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Subcategory <span style="color:red;">*</span></label>
                                    <select name="edit_app_sub_category" id="edit_app_sub_category" class="form-control"><select>
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Keywords</label>
                                    <input type="text" name="edit_app_keywords" id="edit_app_keywords" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Phrases</label>
                                    <input type="text" name="edit_app_phrases" id="edit_app_phrases" class="form-control">
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $("#edit_app_category").on('change',function() {
            $.ajax({
                url: '{{ route('get-sub-categories') }}',
                type: 'POST',
                data: {
                    'category_id': $(this).val(),
                    '_token': "{{csrf_token()}}"
                },
                success: function (response) {
                    $('select[name="edit_app_sub_category"]').empty();
                    $('select[name="edit_app_sub_category"]').append('<option value="">Select Subcategory</option>');
                    $.each(response, function(key, value) {
                        $('select[name="edit_app_sub_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                },
            });
        });

        var aid = null;
        $('.app-edit').on('click',function(e){
            e.preventDefault();
            $('.error').remove();
            let url = $(this).attr("href");
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    aid = response.app.id;
                    // $('select[name="edit_app_sub_category"]').empty();
                    // $('select[name="edit_app_sub_category"]').append('<option value="">Select Subcategory</option>');
                    // $.each(response.subcategories, function(key, value) {
                    //     $('select[name="edit_app_sub_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                    // });
                    $('#edit_app_name').val(response.app.name)
                    $('#edit_app_package_name').val(response.app.package_name)
                    $('#edit_app_operator_name').val(response.app.operator_name)
                    $('#edit_app_operator_email').val(response.app.operator_email)
                    // $('#edit_app_category').val(response.app.category_id)
                    // $('#edit_app_sub_category').val(response.app.sub_category_id)
                    // $('#edit_app_keywords').val(response.app.keywords)
                    // $('#edit_app_phrases').val(response.app.phrases)
                    if(response.app.logo) {
                        $('#edit_app_logo').attr('src', APP_URL+'/upload/app-logo/'+response.app.logo)
                    } else {
                        $('#edit_app_logo').attr('src', APP_URL+'/images/chooseprofile.png')
                    }
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        });

        $('#edit-app-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action')+'/'+aid;
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        $('.app-delete').on('click', function (e) {
            var url = $(this).attr("href");
            var result = confirm('Are you sure you want to delete the app?'); 
            if (result == true) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        '_method': 'delete',
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        window.location.reload();
                    },
                });
            }
            e.preventDefault()
        });
    });

    function filter() {
        var ap = $('#search-app').val();
        var status = $('#search-status').val();
        var url = APP_URL+"/{{Request::segment(1)}}?app="+ap+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection