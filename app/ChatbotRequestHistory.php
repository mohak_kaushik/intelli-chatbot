<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatbotRequestHistory extends Model
{
    protected $table = 'chatbot_request_history';
}
