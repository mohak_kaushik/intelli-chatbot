<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppCategory extends Model
{
    protected $fillable = ['name','status'];
}
