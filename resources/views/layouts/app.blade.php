<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/main/bootstrap.min.css') }}" crossorigin="anonymous" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ asset('css/main/jquery.mCustomScrollbar.css') }}">
    <link href="{{ asset('css/main/style.css') }}" rel="stylesheet">
    @yield('style')
    <title>@yield('title','INTELLI')</title>
</head>
<body>
<div id="loader" >
    <div class="innerloader">
        <span id="ball-1" class="circle"></span>
        <span id="ball-2" class="circle"></span>
        <span id="ball-3" class="circle"></span>
    </div>
</div>

    <header>
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <div id="hamburgericon">
                <img src="{{url('images/hamburgericon.png')}}" alt="">
            </div>
            <a class="navbar-brand" href="#"><img src="{{url('images/iers_logo01.png')}}" alt=""></a>
            <div class="navbar-nav ml-auto">
                <!-- <div class="nav-item dropdown" id="notification">
                    <a class="nav-link dropdown-toggle" href="#" id="notificationbtn" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="notificationbtn">
                        <a class="dropdown-item" href="#">What is Lorem Ipsum?</a>
                        <a class="dropdown-item" href="#">What is Lorem Ipsum?</a>
                    </div>
                </div> -->
                <div class="nav-item dropdown" id="profile">
                    <a class="nav-link dropdown-toggle" href="#" id="userprofile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="profile">
                        @if(Auth::user()->picture!=null)
                            <img src="{{url('upload/user-picture/'.Auth::user()->picture)}}" alt="">
                        @else
                            <img src="{{url('images/loginfeatruedimg.png')}}" alt="">
                        @endif
                        </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="userprofile">
                        <a class="dropdown-item profile-update" href="{{ route('profile.update') }}" data-toggle="modal" data-target="#profile_info">Profile</a>
                        @can('role-list')
                        <a class="dropdown-item" href="{{ route('roles.index') }}">Manage Role</a>
                        @endcan
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <div class="mainbodycontent" id="app">
        <section class="sidebar_maincontent">
            <div class="sidebar">
                @if(Gate::check('app-create') || Gate::check('chatbot-create') || Gate::check('resource-create'))
                <div class="createnew">
                    <button type="button" data-toggle="modal" data-target="#createnew" id="create-new-button">Create New <i class="fas fa-plus"></i></button>
                </div>
                @endif
                <ul class="">
                    <li class="hassubmenu">
                        <a href="{{ route('home') }}" class="{{ (request()->segment(1) == 'home') ? 'active' : '' }}">
                            <i class="fas fa-tachometer-alt"></i>My Dashboard
                        </a>
                        <!-- <span class="carret"><i class="fas fa-angle-down"></i></span>
                        <ul class="submenulist">
                            <li><a href="javascript">Submenu 1</a></li>
                            <li><a href="javascript">Submenu 2</a></li>
                            <li><a href="javascript">Submenu 3</a></li>
                        </ul> -->
                    </li>
                    @can('analytics-list')
                    <li>
                        <a href="{{ route('analytics.index') }}" class="{{ (request()->segment(1) == 'analytics') ? 'active' : '' }}">
                            <i class="fas fa-chart-area"></i>Analytics
                        </a>
                    </li>
                    @endcan
                    @can('app-list')
                    <li>
                        <a href="{{ route('apps.index') }}" class="{{ (request()->segment(1) == 'apps') ? 'active' : '' }}">
                            <i class="fas fa-tablet-alt"></i>Apps
                        </a>
                    </li>
                    @endcan
                    @can('chatbot-list')
                    <li class="hassubmenu">
                        <a href="{{ url('chatbots') }}" class="{{ (request()->segment(1) == 'chatbots') ? 'active' : '' }}">
                            <i class="fas fa-comments"></i>Chatbots
                        </a>
                        <!-- <span class="carret"><i class="fas fa-angle-down"></i></span>
                        <ul class="submenulist">
                            <li><a href="javascript">Submenu 1</a></li>
                            <li><a href="javascript">Submenu 2</a></li>
                            <li><a href="javascript">Submenu 3</a></li>
                        </ul> -->
                    </li>
                    @endcan
                    @can('resource-list')
                    <li>
                        <a href="{{ route('resources.index') }}" class="{{ (request()->segment(1) == 'resources') ? 'active' : '' }}">
                            <i class="fas fa-users"></i>Resources
                        </a>
                    </li>
                    @endcan
                    @can('user-list')
                    <li>
                        <a href="{{ route('users.index') }}" class="{{ (request()->segment(1) == 'users') ? 'active' : '' }}">
                            <i class="fas fa-user"></i>Users
                        </a>
                    </li>
                    @endcan
                    @can('jurisdiction-list')
                    <li>
                        <a href="{{ route('jurisdictions.index') }}" class="{{ (request()->segment(1) == 'jurisdictions') ? 'active' : '' }}">
                            <i class="fas fa-map"></i>Jurisdictions
                        </a>
                    </li>
                    @endcan
                    @can('chatbot-request-history-list')
                    <li>
                        <a href="{{ route('chatbot-request-history') }}" class="{{ (request()->segment(1) == 'chatbot-request-history') ? 'active' : '' }}">
                            <i class="fas fa-history"></i>Chatbot Request History
                        </a>
                    </li>
                    @endcan
                    @can('sender-report-list')
                    <li>
                        <a href="{{ route('sender-reports') }}" class="{{ (request()->segment(1) == 'sender-reports') ? 'active' : '' }}">
                            <i class="far fa-file-alt"></i>Sender Reports
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>

            <main class="py-4">
                @yield('content')
            </main>
        </section>
    </div>

    <!-- Modal -->
    <div class="modal fade " id="createnew" tabindex="-1" role="dialog" aria-labelledby="createnew" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createnew">Create New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert modal-msg" style="display:none;"></div>
                    {!! Form::open(['route'=>'create-new','method'=>'POST','id'=>'create-new-form','files'=>true]) !!}
                    <div class="row logo-field">
                        <div class="col-12">
                            <div class="chooseprofile text-center mb-3">
                                <div class="profile_pic">
                                    <img id="cr_logo" src="{{url('images/chooseprofile.png')}}" alt="" />
                                </div>
                                <div class="upload-btn-wrapper">
                                    <button class=" btn btn-primary">Upload a file</button>
                                    <input type="file" name="logo" onchange="readURL(this, 'cr_logo');" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="formdesign">
                        <div class="row">
                            <div class="col-md-12 cols">
                                <div class="form-group">
                                    <label>Select Any One <span style="color:red;">*</span></label>
                                    <select class="form-control" id="appChatbotResouces" name="create_type">
                                        @can('app-create')
                                        <option value="App">App</option>
                                        @endcan
                                        @can('chatbot-create')
                                        <option value="Chatbot">Chatbot</option>
                                        @endcan
                                        @can('resource-create')
                                        <option value="Resources">Resources</option>
                                        @endcan
                                    </select>
                                </div>
                            </div>
                            <!-- For App Options Fields -->
                            <div class="for_app_fields w-100" id="appfields1">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name <span style="color:red;">*</span></label>
                                        <input type="text" name="app_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Package Name <span style="color:red;">*</span></label>
                                        <input type="text" name="app_package_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Operator Name <span style="color:red;">*</span></label>
                                        <input type="text" name="app_operator_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Operator Email <span style="color:red;">*</span></label>
                                        <input type="text" name="app_operator_email" class="form-control">
                                    </div>
                                </div>
                                <!-- <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Category <span style="color:red;">*</span></label>
                                        <select name="app_category" class="form-control" id="app_category"><select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Subcategory <span style="color:red;">*</span></label>
                                        <select name="app_sub_category" class="form-control"><select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Keywords</label>
                                        <input type="text" name="app_keywords" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Phrases</label>
                                        <input type="text" name="app_phrases" class="form-control">
                                    </div>
                                </div> -->
                            </div>

                            <!-- For Chatbot Option Fields -->
                            <div class="for_chatbot_fields w-100" id="chatbotfields">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>App <span style="color:red;">*</span></label>
                                        <select name="chatbot_app[]" class="selectpicker multipleselectbox cb_app" multiple data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name <span style="color:red;">*</span></label>
                                        <input type="text" name="chatbot_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status <span style="color:red;">*</span></label>
                                        <select name="chatbot_status" class="form-control">
                                        @foreach(Config::get('intelli.chatbot_status') as $csKey => $csVal)
                                            <option value="{{ $csKey }}" @if($csKey=='1') {{ 'selected' }} @endif>{{ $csVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>

                            <!-- For Resouces Option Fields -->
                            <div class="for_chatbot_fields w-100" id="resoucesfields">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>App <span style="color:red;">*</span></label>
                                        <select name="resource_app[]" class="selectpicker multipleselectbox rs_app" multiple data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name <span style="color:red;">*</span></label>
                                        <input type="text" name="resource_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Designation <span style="color:red;">*</span></label>
                                        <input type="text" name="resource_designation" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Authority Level <span style="color:red;">*</span></label>
                                        <select name="resource_authority_level" class="form-control" id="resource_authority_level"><select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Jurisdiction <span style="color:red;">*</span></label>
                                        <select name="resource_jurisdiction[]" class="selectpicker multipleselectbox rs_jurisdiction" multiple data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Category <span style="color:red;">*</span></label>
                                        <select name="resource_category" class="form-control" id="resource_category"><select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Subcategory <span style="color:red;">*</span></label>
                                        <select name="resource_sub_category" class="form-control"><select>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Phrases</label>
                                        <input type="text" name="resource_phrases" class="form-control">
                                    </div>
                                </div> -->
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Max Unit of Services</label>
                                        <input type="text" name="resource_max_unit_of_service" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Resource Provision</label>
                                        <input type="text" name="resource_provision" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Refresh Time</label>
                                        <select name="resource_refresh_time" class="form-control">
                                            <option value="">Select Refresh Time</option>
                                        @foreach(Config::get('intelli.resource_refresh_time') as $rtKey => $rtVal)
                                            <option value="{{ $rtKey }}" @if($rtKey=='1') {{ 'selected' }} @endif>{{ $rtVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Contact Number <span style="color:red;">*</span></label>
                                        <input type="text" name="resource_contact_no" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Address </label>
                                        <input type="text" name="resource_address" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Email <span style="color:red;">*</span></label>
                                        <input type="email" name="resource_email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Website </label>
                                        <input type="text" name="resource_website" class="form-control">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="profile_info" tabindex="-1" role="dialog" aria-labelledby="profile_info" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert modal-msg" style="display:none;"></div>
                    {!! Form::open(['route'=>'profile.update','method'=>'POST','id'=>'profile-form','files'=>true]) !!}
                    <div class="formdesign">
                        <div class="row">
                            <div class="col-12">
                                <div class="chooseprofile text-center mb-3">
                                    <div class="profile_pic">
                                        <img id="user_picture" src="{{url('images/chooseprofile.png')}}" alt="" />
                                    </div>
                                    <div class="upload-btn-wrapper">
                                        <button class=" btn btn-primary">Upload a file</button>
                                        <input type="file" name="profile_picture" onchange="readURL(this, 'user_picture');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Name <span style="color:red;">*</span></label>
                                    <input type="text" name="profile_name" id="profile_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Email <span style="color:red;">*</span></label>
                                    <input type="email" name="profile_email" id="profile_email" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="profile_password" id="profile_password" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 cols float-left">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" name="profile_confirm_password" id="profile_confirm_password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/main/jquery-3.3.1.slim.min.js') }}" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="{{ asset('js/main/popper.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('js/main/bootstrap.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('js/main/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="{{ asset('js/main/custom.js') }}"></script>

    <script>
    var APP_URL = {!! json_encode(url('/')) !!}
    $(document).ready(function(){
        var appPms = "{{ Gate::check('app-create') }}";
        var cbPms = "{{ Gate::check('chatbot-create') }}";
        var rPms = "{{ Gate::check('resource-create') }}";

        $(".selectpicker").selectpicker({
            noneSelectedText : 'Please Select' // by this default 'Nothing selected' -->will change to Please Select
        });

        // on changes of app/chatbot/resouces on modal
        $('#appChatbotResouces').on('change',function(){
            $('.error').remove();
            var currentText = $(this).val();
            if(currentText == 'App'){
                $('#appfields1, .logo-field').show();
                $('#chatbotfields, #resoucesfields').hide();
            }else if(currentText == 'Chatbot') {
                $('#chatbotfields').show();
                $('#appfields1, #resoucesfields, .logo-field').hide();
                //Get App List
                $.ajax({
                    url: '{{ route('get-apps') }}',
                    type: 'GET',
                    success: function (response) {
                        $('select[name="chatbot_app[]"]').empty();
                        $.each(response, function(key, value) {
                            $('select[name="chatbot_app[]"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    },
                });
            }else if(currentText == 'Resources') {
                $('#resoucesfields, .logo-field').show();
                $('#appfields1, #chatbotfields').hide();
                $.ajax({
                    url: '{{ route('get-resource-field-data') }}',
                    type: 'GET',
                    success: function (response) {
                        //Select App 
                        $('select[name="resource_app[]"]').empty();
                        $.each(response.apps, function(key, value) {
                            $('select[name="resource_app[]"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        //Select Jurisdiction
                        $('select[name="resource_jurisdiction[]"]').empty();
                        $.each(response.jurisdictions, function(key, value) {
                            $('select[name="resource_jurisdiction[]"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                        //Select Category
                        $('select[name="resource_category"]').empty();
                        $('select[name="resource_category"]').append('<option value="">Select Category</option>');
                        $.each(response.categories, function(key, value) {
                            $('select[name="resource_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        //Select Authority
                        $('select[name="resource_authority_level"]').empty();
                        $('select[name="resource_authority_level"]').append('<option value="">Select Authority Level</option>');
                        $.each(response.authority_level, function(key, value) {
                            $('select[name="resource_authority_level"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                    },
                });
            }
        });

        $("#resource_category").on('change',function() {
            $.ajax({
                url: '{{ route('get-resource-sub-categories') }}',
                type: 'POST',
                data: {
                    'category_id': $(this).val(),
                    '_token': "{{csrf_token()}}"
                },
                success: function (response) {
                    $('select[name="resource_sub_category"]').empty();
                    $('select[name="resource_sub_category"]').append('<option value="">Select Subcategory</option>');
                    $.each(response, function(key, value) {
                        $('select[name="resource_sub_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                },
            });
        });

        $("#create-new-button").on('click',function() {
            var currentPage = "{{ request()->segment(1) }}";
            $('.error').remove();
            $('#create-new-form').trigger("reset");
            if(currentPage == 'apps' && appPms) {
                appFields();
            } else if(currentPage == 'chatbots' && cbPms) {
                chatbotFields();
            } else if(currentPage == 'resources' && rPms) {
                resourceFields();
            } else {
                if(appPms) {
                    appFields();
                } else if(cbPms) {
                    chatbotFields();
                } else {
                    resourceFields();
                }
            }
        });

        $("#app_category").on('change',function() {
            $.ajax({
                url: '{{ route('get-sub-categories') }}',
                type: 'POST',
                data: {
                    'category_id': $(this).val(),
                    '_token': "{{csrf_token()}}"
                },
                success: function (response) {
                    $('select[name="app_sub_category"]').empty();
                    $('select[name="app_sub_category"]').append('<option value="">Select Subcategory</option>');
                    $.each(response, function(key, value) {
                        $('select[name="app_sub_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                },
            });
        });

        $('#create-new-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action');
            $('.error').remove()
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        if(key=='chatbot_app') {
                            $(".cb_app:first").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        } else if(key=='resource_app') {
                            $(".rs_app:first").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        } else if(key=='resource_jurisdiction') {
                            $(".rs_jurisdiction:first").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        } else {
                            $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        }
                        
                    })
                },
            });
        });

        $('.profile-update').on('click',function(e){
            e.preventDefault();
            $('.error').remove();
            $('#profile-form').trigger("reset");
            let url = $(this).attr("href");
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    $('#profile_name').val(response.name)
                    $('#profile_email').val(response.email)
                    if(response.picture != null)
                        $('#user_picture').attr('src', APP_URL+'/upload/user-picture/'+response.picture)
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        });

        $('#profile-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action');
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

    });

    function readURL(input, imgid) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+imgid).attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function appFields() {
        $('#appChatbotResouces').val('App');
        $('#appfields1, .logo-field').show();
        $('#chatbotfields, #resoucesfields').hide();
        // $.ajax({
        //     url: '{{ route('get-categories') }}',
        //     type: 'GET',
        //     success: function (response) {
        //         $('select[name="app_category"]').empty();
        //         $('select[name="app_category"]').append('<option value="">Select Category</option>');
        //         $.each(response, function(key, value) {
        //             $('select[name="app_category"]').append('<option value="'+ key +'">'+ value +'</option>');
        //         });
        //     },
        // });
    }

    function chatbotFields() {
        $('#appChatbotResouces').val('Chatbot');
        $('#chatbotfields').show();
        $('#appfields1, #resoucesfields, .logo-field').hide();
        //Get App List
        $.ajax({
            url: '{{ route('get-apps') }}',
            type: 'GET',
            success: function (response) {
                $('select[name="chatbot_app[]"]').empty();
                $.each(response, function(key, value) {
                    $('select[name="chatbot_app[]"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
                $('.selectpicker').selectpicker('refresh');
            },
        });
    }

    function resourceFields() {
        $('#appChatbotResouces').val('Resources');
        $('#resoucesfields, .logo-field').show();
        $('#appfields1, #chatbotfields').hide();
        $.ajax({
            url: '{{ route('get-resource-field-data') }}',
            type: 'GET',
            success: function (response) {
                //Select App 
                $('select[name="resource_app[]"]').empty();
                $.each(response.apps, function(key, value) {
                    $('select[name="resource_app[]"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
                //Select Jurisdiction
                $('select[name="resource_jurisdiction[]"]').empty();
                $.each(response.jurisdictions, function(key, value) {
                    $('select[name="resource_jurisdiction[]"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
                $('.selectpicker').selectpicker('refresh');
                //Select Category
                $('select[name="resource_category"]').empty();
                $('select[name="resource_category"]').append('<option value="">Select Category</option>');
                $.each(response.categories, function(key, value) {
                    $('select[name="resource_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
                //Select Authority
                $('select[name="resource_authority_level"]').empty();
                $('select[name="resource_authority_level"]').append('<option value="">Select Authority Level</option>');
                $.each(response.authority_level, function(key, value) {
                    $('select[name="resource_authority_level"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
            },
        });
    }
    </script>

    @yield('script')
</body>
</html>
