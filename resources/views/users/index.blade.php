@extends('layouts.app')
@section('title', 'Users')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Users</h4>
        @can('user-create')
        <a class="titlebtn" href="javascript:void(0);" data-toggle="modal" data-target="#addUserModal"><i class="fas fa-plus"></i> Add User </a>
        @endcan
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="mb-4 float-left filters-input">
        <input type="text" name="search-user" id="search-user" placeholder="Search" onchange="filter()" value="{{app('request')->input('user')}}">
        <select name="search-role" id="search-role" onchange="filter()">
            <option value="">Select role</option>
            @foreach($roles as $rKey => $rVal)
                <option value="{{ $rKey }}" @if(request()->filled('role') && $rKey==app('request')->input('role')) {{ 'selected' }} @endif>{{ $rVal }}</option>
            @endforeach
        <select>
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Sr. No.</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Last Modified</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center" width="200">Action</th>
                </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td>{{ (($users->currentpage()-1)*$users->perPage()) + $loop->index+1 }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                            <label class="badge badge-success">{{ $v }}</label>
                            @endforeach
                        @endif
                    </td>
                    <td>{{ $user->creator->name }}</td>
                    <td>
                        @if($user->last_updated_by)
                            {{ \Carbon\Carbon::parse($user->updated_at)->format('d M Y')}}
                        @else
                            {{ \Carbon\Carbon::parse($user->created_at)->format('d M Y')}}
                        @endif
                    </td>
                    <td>
                        @if($user->status == '1')
                            <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                        @else 
                            <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                        @endif
                    </td>
                    <td class="text-center">
                        @can('user-edit')
                        <a href="{{ route('users.edit',$user->id) }}" class="user-edit" data-toggle="modal" data-target="#editUserModal"><i class="fas fa-edit"></i></a>
                        @endcan
                        @can('user-delete')
                        <a href="{{ route('users.destroy',$user->id) }}" class="user-delete"><i class="fas fa-trash-alt"></i></a>
                        @endcan
                    </td>
                </tr>
            @empty
                <tr><td colspan="7" align="center">No data found</td></tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$users->appends(['user' => app('request')->input('user'), 'role' => app('request')->input('role'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>

<!-- Add User Modal -->
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>'users.store','method'=>'POST','id'=>'add-user-form','files'=>true]) !!}
                    <div class="formdesign">
                        <div class="row logo-field">
                            <div class="col-12">
                                <div class="chooseprofile text-center mb-3">
                                    <div class="profile_pic">
                                        <img id="add_user_picture" src="{{url('images/chooseprofile.png')}}" alt="" />
                                    </div>
                                    <div class="upload-btn-wrapper">
                                        <button class=" btn btn-primary">Upload a file</button>
                                        <input type="file" name="add_user_picture" onchange="readURL(this, 'add_user_picture');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="w-100">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="add_user_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Email </label>
                                        <input type="text" name="add_user_email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="add_user_password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="add_user_confirm_password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Role</label>
                                        {!! Form::select('add_user_role', $roles, null, array('class' => 'form-control', 'placeholder' => 'Select Role')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status </label>
                                        <select name="add_user_status" class="form-control">
                                        @foreach(Config::get('intelli.status') as $sKey => $sVal)
                                            <option value="{{ $sKey }}" @if($sKey=='1') {{ 'selected' }} @endif>{{ $sVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Edit User Modal -->
<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="editUserModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>['users.update', ''],'method'=>'PUT','id'=>'edit-user-form','files'=>true]) !!}
                    <div class="formdesign">
                        <div class="row logo-field">
                            <div class="col-12">
                                <div class="chooseprofile text-center mb-3">
                                    <div class="profile_pic">
                                        <img id="edit_user_picture" src="{{url('images/chooseprofile.png')}}" alt="" />
                                    </div>
                                    <div class="upload-btn-wrapper">
                                        <button class="btn btn-primary">Upload a file</button>
                                        <input type="file" name="edit_user_picture" onchange="readURL(this, 'edit_user_picture');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="w-100">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="edit_user_name" id="edit_user_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Email </label>
                                        <input type="text" name="edit_user_email" id="edit_user_email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="edit_user_password" id="edit_user_password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="edit_user_confirm_password" id="edit_user_confirm_password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Role</label>
                                        {!! Form::select('edit_user_role', $roles, null, array('id' => 'edit_user_role', 'class' => 'form-control', 'placeholder' => 'Select Role')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status </label>
                                        <select name="edit_user_status" id="edit_user_status" class="form-control">
                                        @foreach(Config::get('intelli.status') as $sKey => $sVal)
                                            <option value="{{ $sKey }}">{{ $sVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        //User add
        $('#add-user-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action');
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                    error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        //User edit
        var uid = null;
        $('.user-edit').on('click',function(e){
            e.preventDefault();
            let url = $(this).attr("href");
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    uid = response.user.id;
                    $('#edit_user_name').val(response.user.name)
                    $('#edit_user_email').val(response.user.email)
                    $('#edit_user_role').val(response.roles[Object.keys(response.roles)[0]])
                    $('#edit_user_status').val(response.user.status)
                    if(response.user.picture){
                        $('#edit_user_picture').attr('src', APP_URL+'/upload/user-picture/'+response.user.picture)
                    } else {
                        $('#edit_user_picture').attr('src', APP_URL+'/images/chooseprofile.png')
                    }
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        })

        //User update
        $('#edit-user-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action')+'/'+uid;
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        //User delete
        $('.user-delete').on('click', function (e) {
            var url = $(this).attr("href");
            var result = confirm('Are you sure you want to delete the user?'); 
            if (result == true) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        '_method': 'delete',
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        window.location.reload();
                    },
                });
            }
            e.preventDefault()
        });
    });

    function filter() {
        var usr = $('#search-user').val();
        var status = $('#search-status').val();
        var role = $('#search-role').val();
        var url = APP_URL+"/{{Request::segment(1)}}?user="+usr+"&role="+role+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection
