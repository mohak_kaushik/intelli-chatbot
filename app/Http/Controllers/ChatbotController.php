<?php

namespace App\Http\Controllers;

use App\Chatbot;
use App\App;
use App\ChatbotRequestHistory;
use Illuminate\Http\Request;
use Validator;
use Response;
use Auth;
use Request as Req;
use Carbon\Carbon;
use App\Activity;

class ChatbotController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:chatbot-list');
        $this->middleware('permission:chatbot-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:chatbot-delete', ['only' => ['destroy']]);
        $this->middleware('permission:chatbot-request-history-list', ['only' => ['requestHistory']]);
    }
    
    public function index(Request $request)
    {
        $apps = App::where('status','1')->pluck('name', 'id')->toArray();
        $query = Chatbot::query();
        if(isset($request->chatbot) && $request->chatbot!='') {
            $query->where('name', 'like', '%' . $request->chatbot . '%');
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }
        $chatbots = $query->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited chatbot list page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('chatbots', compact('chatbots', 'apps'));
    }

    public function edit(Chatbot $chatbot)
    {
        return ['chatbot' => $chatbot, 'apps' => $chatbot->apps->pluck('id')];
    }

    public function update(Request $request, Chatbot $chatbot)
    {
        $validator = Validator::make($request->all(), [
            'edit_chatbot_app' => 'required',
            'edit_chatbot_name' => 'required',
            'edit_chatbot_status' => 'required',
        ],[
            'edit_chatbot_app.required'=>'The app field is required.',
            'edit_chatbot_name.required'=>'The name field is required.',
            'edit_chatbot_status.required'=>'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $chatbot->name = $request->edit_chatbot_name;
        $chatbot->status = $request->edit_chatbot_status;
        $chatbot->last_updated_by = Auth::user()->id;
        $chatbot->save();
        $chatbot->apps()->detach();
        $chatbot->apps()->attach($request->edit_chatbot_app);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated chatbot details.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('Chatbot updated successfully.', 200);
    }

    public function requestHistory(Request $request)
    {
        $query = ChatbotRequestHistory::query();
        if(isset($request->sender) && $request->sender!='') {
            $query->where('sender_id', 'like', '%' . $request->sender . '%');
        }
        if(isset($request->category) && $request->category!='') {
            $query->where('bot_category', 'like', '%' . $request->category . '%');
        }
        if(isset($request->subcategory) && $request->subcategory!='') {
            $query->where('bot_sub_category', 'like', '%' . $request->subcategory . '%');
        }
        if(isset($request->date) && ($request->date!='')) {
            $date_range = explode(' - ',$request->date);
            $sdate = Carbon::createFromFormat('d/m/Y', $date_range[0])->format('Y-m-d');
            $edate = Carbon::createFromFormat('d/m/Y', $date_range[1])->format('Y-m-d');
            $query->where('created_at', '>=', $sdate." 00:00:00")->where('created_at', '<=',  $edate." 23:59:59");
        }
        $requestHistory = $query->orderBy('created_at','DESC')->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited chatbot request history page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('chatbot-request-history', compact('requestHistory'));
    }

    public function destroy(Chatbot $chatbot)
    {
        $chatbot->apps()->detach();
        $chatbot->delete();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Deleted Chatbot.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'Chatbot deleted successfully.');
        return response()->json('Chatbot deleted successfully.', 200);
    }

}
