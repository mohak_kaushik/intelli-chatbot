<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('picture')->nullable()->after('remember_token');
            $table->bigInteger('created_by')->unsigned()->index()->after('picture');
            $table->bigInteger('last_updated_by')->unsigned()->index()->nullable()->after('created_by');
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive, 1 => Active')->after('last_updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('picture');
            $table->dropColumn('created_by');
            $table->dropColumn('last_updated_by');
        });
    }
}
