@extends('layouts.app')
@section('title', 'Analytics')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Analytics</h4>
    </div>

    <div class="mb-4 filters-input">
        <input type="text" name="request_datetime" id="search-date" placeholder="dd/mm/yyyy - dd/mm/yyyy" value="@if($startDate) {{$startDate . ' - ' . $endDate}} @endif">
    </div>

    <div class="row">
        <div class="col-12">
            <div class="fullwidthcart w-100 position-relative">
                <div class="titlestrip mb-4">
                    <h4>Data of Location</h4>
                </div>

                <div class="mt-5 mb-4">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Country </th>
                                <th scope="col">Category </th>
                                <th scope="col">Subcategory</th>
                                <th scope="col">Count </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($analyticsData as $data)
                            <tr>
                                <td>{{ $data->sender_country }}</td>
                                <td>{{ $data->bot_category }}</th>
                                <td>{{ $data->bot_sub_category }}</td>
                                <td>{{ $data->total }}</td>
                            </tr>
                            @empty
                                <tr><td colspan="4" align="center">No data found</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div>
                    {{$analyticsData->appends(['date' => app('request')->input('date')])->links()}}
                </div>
            </div>
        </div>
    </div>

    <div class="locationmap-progressbar my-4 ">
        <div class="row">
            <div class="col-12">
                <div class="fullwidthcart w-100 position-relative">
                    <div class="titlestrip mb-4">
                        <h4>Locations</h4>
                    </div>
                </div>
            </div>
            <div class="col-8 py-3">
                <div class="fullwidthcart w-100">
                    <div id="basic_choropleth" class="locationcart"></div>
                </div>
            </div>

            <div class="col-4 py-3">
                @foreach($analyticsGraphData as $data)
                <div class="progress-lable">
                    <label>{{ $data['country'] }}</label>
                    <div class="progress mb-3">
                        <div class="progress-bar progress-bar-striped" style="width:{{ $data['count'] }}%;background-color: {{ $data['color'] }};">{{ $data['count'] }}%
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
<script src="https://datamaps.github.io/scripts/datamaps.world.min.js"></script>

<script>
var analyticsMapData = <?php echo json_encode($analyticsMapData) ?>;

// For Locations
var basic_choropleth = new Datamap({
  element: document.getElementById("basic_choropleth"),
  projection: 'mercator',
  geographyConfig: {
    popupTemplate: function(geography, data) {
        if(data != null) {
            return `<div class="hoverinfo">` + geography.properties.name + ` 
                - ` +  data.count + `%`
        } else {
            return `<div class="hoverinfo">` + geography.properties.name
        }
    },
  },
  fills: {
    defaultFill: "#9a9ca6",
    cntry_1: "#c81a1a",
    cntry_2: "#808b33",
    cntry_3: "#f87434",
    cntry_4: "#3459f8",
    cntry_5: "#006666",
  },
  data: analyticsMapData
});

$(document).ready(function(){
    $('input[name="request_datetime"]').daterangepicker({
        opens: 'right',
        autoUpdateInput: false,
        locale: {
            format: "DD/MM/YYYY"
        },
        autoApply: true
    });

    $('input[name="request_datetime"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        var requestdate = $('#search-date').val();
        var url = APP_URL+"/{{Request::segment(1)}}?date="+requestdate;
        if (url) {
            window.location = url;
        }
    });
});
</script>
@endsection
