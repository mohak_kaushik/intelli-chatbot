<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AppCategoriesTableSeeder::class);
        $this->call(AppSubCategoriesTableSeeder::class);
        $this->call(ResourceCategoriesTableSeeder::class);
        $this->call(ResourceSubCategoriesTableSeeder::class);
        $this->call(ResourceAuthorityLevelsTableSeeder::class);
    }
}
