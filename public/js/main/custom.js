$(document).ready(function(){
    $('#hamburgericon').on('click', function(){
        $('body').toggleClass('closemenu');
    });

    // Submenu Script
    $('.sidebar ul .hassubmenu .carret').on('click', function(){
        $(this).parents('.hassubmenu').find('.submenulist').slideToggle();
    });
    $('body').on('click', function(event){
        if(!($(event.target).closest('.sidebar, #hamburgericon').length > 0)) {
            $('body').removeClass('closemenu');
        }
    });

});

$(window).on('load', function(){
    $('#loader').hide();
});