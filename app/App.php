<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function chatbots() 
    {
        return $this->belongsToMany('App\Chatbot', 'app_chatbot', 'app_id', 'chatbot_id');
    }

    public function resources() 
    {
        return $this->belongsToMany('App\Resource', 'app_resource', 'app_id', 'resource_id');
    }
}
