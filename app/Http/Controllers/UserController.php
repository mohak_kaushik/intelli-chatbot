<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Activity;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use Request as Req;
use Validator;
use Response;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list', ['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $query = User::query();
        if(isset($request->user) && $request->user!='') {
            $query->where(function ($qry) use($request) { 
                $qry->where('name', 'like', '%' . $request->user . '%')
                ->orWhere('email', 'like', '%' . $request->user . '%');
            });
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }
        if(isset($request->role) && $request->role!='') {
            $query->role([$request->role]);
        }
        
        $users = $query->where('id', '!=', auth()->user()->id)->paginate(10);
        $roles = Role::pluck('name','name')->all();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited user list page.';
        $activity->user_id = Auth::user()->id;
        $activity->save();

        return view('users.index', compact('users','roles'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'add_user_name' => 'required',
            'add_user_picture' => 'mimes:jpeg,jpg,png',
            'add_user_email' => 'required|email|unique:users,email',
            'add_user_password' => 'required|same:add_user_confirm_password',
            'add_user_role' => 'required',
            'add_user_status' => 'required',
        ],[
            'add_user_name.required' => 'The name field is required.',
            // 'add_user_picture.required' => 'The picture field is required.',
            'add_user_picture.mimes' => 'The picture must be a file of type: jpeg, jpg, png.',
            'add_user_email.required' => 'The email field is required.',
            'add_user_email.email' => 'The email must be a valid email address.',
            'add_user_email.unique' => 'The email has already been taken.',
            'add_user_password.required' => 'The password field is required.',
            'add_user_password.same' => 'The password and confirm password must match.',
            'add_user_role.required' => 'The role field is required.',
            'add_user_status.required' => 'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $user = new User();
        if ($request->hasFile('add_user_picture')) {  
            if (!file_exists('upload/user-picture/')) {
                mkdir('upload/user-picture/', 0777, true);
            }
            
            $picture = time().'.'.request()->add_user_picture->getClientOriginalExtension();
            request()->add_user_picture->move(public_path('upload/user-picture'), $picture);
            $user->picture = $picture;
        }

       
        $user->name = $request->add_user_name;
        $user->email = $request->add_user_email;
        $user->password = Hash::make($request->add_user_password);
        $user->status = $request->add_user_status;
        $user->created_by = Auth::user()->id;
        $user->save();
        $user->assignRole([$request->add_user_role]);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new user.';
        $activity->user_id = Auth::user()->id;
        $activity->save();

        return response()->json('User created successfully.', 200);
    }

    public function edit(User $user)
    {
        $userRole = $user->roles->pluck('name','name')->all();
        return ['user' => $user, 'roles' => $userRole];
    }


    public function update(Request $request, User $user)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'edit_user_name' => 'required',
            'edit_user_picture' => 'mimes:jpeg,jpg,png',
            'edit_user_email' => 'required|email|unique:users,email,'.$user->id,
            'edit_user_password' => 'same:edit_user_confirm_password',
            'edit_user_role' => 'required',
            'edit_user_status' => 'required',
        ],[
            'edit_user_name.required' => 'The name field is required.',
            'edit_user_picture.mimes' => 'The picture must be a file of type: jpeg, jpg, png.',
            'edit_user_email.required' => 'The email field is required.',
            'edit_user_email.email' => 'The email must be a valid email address.',
            'edit_user_email.unique' => 'The email has already been taken.',
            'edit_user_password.same' => 'The password and confirm password must match.',
            'edit_user_role.required' => 'The role field is required.',
            'edit_user_status.required' => 'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        if ($request->hasFile('edit_user_picture')) {  
            if ((($user->picture!=NULL) || ($user->picture!='')) && (\File::exists(public_path('upload/user-picture/'.$user->picture)))) {
                unlink(public_path('upload/user-picture/'.$user->picture));
            }
            $picture = time().'.'.request()->edit_user_picture->getClientOriginalExtension();
            request()->edit_user_picture->move(public_path('upload/user-picture'), $picture);
            $user->picture = $picture;
        }

        $user->name = $request->edit_user_name;
        $user->email = $request->edit_user_email;
        if($request->edit_user_password)
            $user->password = Hash::make($request->edit_user_password);
        $user->status = $request->edit_user_status;
        $user->last_updated_by = Auth::user()->id;
        $user->save();
        DB::table('model_has_roles')->where('model_id',$user->id)->delete();
        $user->assignRole([$request->edit_user_role]);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated user details.';
        $activity->user_id = Auth::user()->id;
        $activity->save();

        return response()->json('User updated successfully.', 200);
    }

    public function destroy(User $user)
    {
        if ((($user->picture!=NULL) || ($user->picture!='')) && (\File::exists(public_path('upload/user-picture/'.$user->picture)))) {
            unlink(public_path('upload/user-picture/'.$user->picture));
        }
        $user->delete();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Deleted user.';
        $activity->user_id = Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'User deleted successfully.');
        return response()->json('User deleted successfully.', 200);
    }

    public function profileUpdate(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if(Req::isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'profile_picture' => 'mimes:jpeg,jpg,png',
                'profile_name' => 'required',
                'profile_email' => 'required|email|unique:users,email,'.$user->id,
                'profile_password' => 'same:profile_confirm_password',
            ],[
                'profile_picture.mimes' => 'The picture must be a file of type: jpeg, jpg, png.',
                'profile_name.required' => 'The name field is required.',
                'profile_email.required' => 'The email field is required.',
                'profile_email.email' => 'The email must be a valid email address.',
                'profile_email.unique' => 'The email has already been taken.',
                'profile_password.same' => 'The password and confirm password must match.',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }

            if ($request->hasFile('profile_picture')) {  
                if (($user->picture!=NULL) && (\File::exists(public_path('upload/user-picture/'.$user->picture)))) {
                    unlink(public_path('upload/user-picture/'.$user->picture));
                }
                $picture = time().'.'.request()->profile_picture->getClientOriginalExtension();
                request()->profile_picture->move(public_path('upload/user-picture'), $picture);
                $user->picture = $picture;
            }
    
            $user->name = $request->profile_name;
            $user->email = $request->profile_email;
            if($request->profile_password)
                $user->password = Hash::make($request->profile_password);
            $user->last_updated_by = Auth::user()->id;
            $user->save();

            //Save activity
            $activity = new Activity();
            $activity->activity = 'Updated my profile details.';
            $activity->user_id = Auth::user()->id;
            $activity->save();

            return response()->json('Profile updated successfully.', 200);
        }
        return $user;
    }
}