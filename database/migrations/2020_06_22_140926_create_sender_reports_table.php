<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSenderReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sender_reports', function (Blueprint $table) {
            $table->id();
            $table->string('sender_id',100);
            $table->string('info_type');
            $table->string('info_report')->nullable();
            $table->string('info_image')->nullable();
            $table->string('category');
            $table->string('sub_category');
            $table->bigInteger('app_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sender_reports');
    }
}
