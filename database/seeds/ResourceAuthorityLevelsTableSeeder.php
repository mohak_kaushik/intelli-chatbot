<?php

use Illuminate\Database\Seeder;
use App\ResourceAuthorityLevel;

class ResourceAuthorityLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            'Level 1',
            'Level 2',
            'Level 3'
        ];
 
        foreach ($levels as $level) {
            ResourceAuthorityLevel::create(['name' => $level]);
        }
    }
}
