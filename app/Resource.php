<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function apps()
    {
        return $this->belongsToMany('App\App', 'app_resource', 'resource_id', 'app_id');
    }

    public function jurisdictions()
    {
        return $this->belongsToMany('App\Jurisdiction', 'jurisdiction_resource', 'resource_id', 'jurisdiction_id');
    }
}
