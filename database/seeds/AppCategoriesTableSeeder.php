<?php

use Illuminate\Database\Seeder;
use App\AppCategory;

class AppCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'App Category 1',
            'App Category 2',
            'App Category 3'
        ];
 
        foreach ($categories as $category) {
            AppCategory::create(['name' => $category]);
        }
    }
}
