<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<link href="{{asset('css/main/bootstrap.min.css')}}" rel="stylesheet" crossorigin="anonymous">
<link href="{{asset('css/main/style.css')}}" rel="stylesheet">
<title>INTELLI | Login</title>
</head>
<body>

<section class="loginsection position-relative">
    <div class="container-fluid h-100 logincontainer">
        <div class="row h-100">
            <div class="col-md-8 featured-image">
                <div class="row h-100">
                    <div class="col-12 align-self-end featruedcontent">
                        <img src="{{url('images/iers_logo.png')}}">
                        <!-- <h1>Chat Bot Management System</h1> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4 login-form">
                <div class="row h-100">
                    <div class="col-12 align-self-center">
                        <div class="formparent">
                            <img class="logo-login" src="{{url('images/featured-logo.png')}}">
                            <form method="POST" action="{{ route('password.email') }}" autocomplete="off">
                                @csrf
                                <h5 class=mb-3>Forgot Password</h5>
                                <p>Please enter your email id so that we can send you the link to reset password.</p>
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <i class="fas fa-envelope"></i>
                                    <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary mt-4 restbtnstyle">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                                <div class="form-group form-check p-0 py-3 text-center">
                                    <label class="form-check-label"><a href="{{url('login')}}">Back to SingIn?</a></label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('js/main/jquery-3.3.1.slim.min.js') }}" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="{{ asset('js/main/popper.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ asset('js/main/bootstrap.min.js') }}" crossorigin="anonymous"></script>
</body>
</html>
