<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurisdictionResource extends Model
{
    protected $fillable = ['jurisdiction_id','resource_id'];
}
