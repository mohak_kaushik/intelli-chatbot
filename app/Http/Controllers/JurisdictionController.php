<?php

namespace App\Http\Controllers;

use App\Jurisdiction;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Response;
use Request as Req;
use App\Activity;

class JurisdictionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:jurisdiction-list');
        $this->middleware('permission:jurisdiction-create', ['only' => ['store']]);
        $this->middleware('permission:jurisdiction-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:jurisdiction-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Jurisdiction::query();
        if(isset($request->jurisdiction) && $request->jurisdiction!='') {
            $query->where('name', 'like', '%' . $request->jurisdiction . '%');
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }

        $jurisdictions = $query->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited jurisdiction list page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('jurisdiction', compact('jurisdictions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nameid' => 'required|unique:jurisdictions,name',
            'pointonelat' => 'required',
            'pointonelng' => 'required',
            'pointtwolat' => 'required',
            'pointtwolng' => 'required',
            'pointthreelat' => 'required',
            'pointthreelng' => 'required',
            'pointfourlat' => 'required',
            'pointfourlng' => 'required',
        ],[
            'nameid.required'=>'The jurisdiction id field is required.',
            'nameid.unique'=>'This jurisdiction id already taken.',
            'pointonelat.required'=>'The latitude field is required.',
            'pointonelng.required'=>'The longitude field is required.',
            'pointtwolat.required'=>'The latitude field is required.',
            'pointtwolng.required'=>'The longitude field is required.',
            'pointthreelat.required'=>'The latitude field is required.',
            'pointthreelng.required'=>'The longitude field is required.',
            'pointfourlat.required'=>'The latitude field is required.',
            'pointfourlng.required'=>'The longitude field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $jurisdiction = new Jurisdiction();
        $jurisdiction->name = $request->nameid;
        $jurisdiction->min_lat = $request->minlatitude;
        $jurisdiction->max_lat = $request->maxlatitude;
        $jurisdiction->min_lng = $request->minlongitude;
        $jurisdiction->max_lng = $request->maxlongitude;
        $jurisdiction->point1_lat = $request->pointonelat;
        $jurisdiction->point1_lng = $request->pointonelng;
        $jurisdiction->point2_lat = $request->pointtwolat;
        $jurisdiction->point2_lng = $request->pointtwolng;
        $jurisdiction->point3_lat = $request->pointthreelat;
        $jurisdiction->point3_lng = $request->pointthreelng;
        $jurisdiction->point4_lat = $request->pointfourlat;
        $jurisdiction->point4_lng = $request->pointfourlng;
        $jurisdiction->created_by = Auth::user()->id;
        $jurisdiction->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new jurisdiction.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'New jurisdiction created successfully.');
        return response()->json('New jurisdiction created successfully.', 200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jurisdiction  $jurisdiction
     * @return \Illuminate\Http\Response
     */
    public function edit(Jurisdiction $jurisdiction)
    {
        return $jurisdiction;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurisdiction  $jurisdiction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jurisdiction $jurisdiction)
    {
        $validator = Validator::make($request->all(), [
            'nameid' => 'required|unique:jurisdictions,name,'.$jurisdiction->id,
            'pointonelat' => 'required',
            'pointonelng' => 'required',
            'pointtwolat' => 'required',
            'pointtwolng' => 'required',
            'pointthreelat' => 'required',
            'pointthreelng' => 'required',
            'pointfourlat' => 'required',
            'pointfourlng' => 'required',
        ],[
            'nameid.required'=>'The jurisdiction id field is required.',
            'nameid.unique'=>'This jurisdiction id already taken.',
            'pointonelat.required'=>'The latitude field is required.',
            'pointonelng.required'=>'The longitude field is required.',
            'pointtwolat.required'=>'The latitude field is required.',
            'pointtwolng.required'=>'The longitude field is required.',
            'pointthreelat.required'=>'The latitude field is required.',
            'pointthreelng.required'=>'The longitude field is required.',
            'pointfourlat.required'=>'The latitude field is required.',
            'pointfourlng.required'=>'The longitude field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $jurisdiction->name = $request->nameid;
        $jurisdiction->min_lat = $request->minlatitude;
        $jurisdiction->max_lat = $request->maxlatitude;
        $jurisdiction->min_lng = $request->minlongitude;
        $jurisdiction->max_lng = $request->maxlongitude;
        $jurisdiction->point1_lat = $request->pointonelat;
        $jurisdiction->point1_lng = $request->pointonelng;
        $jurisdiction->point2_lat = $request->pointtwolat;
        $jurisdiction->point2_lng = $request->pointtwolng;
        $jurisdiction->point3_lat = $request->pointthreelat;
        $jurisdiction->point3_lng = $request->pointthreelng;
        $jurisdiction->point4_lat = $request->pointfourlat;
        $jurisdiction->point4_lng = $request->pointfourlng;
        $jurisdiction->last_updated_by = Auth::user()->id;
        $jurisdiction->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated jurisdiction details.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'Jurisdiction updated successfully.');
        return response()->json('Jurisdiction updated successfully.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jurisdiction  $jurisdiction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jurisdiction $jurisdiction)
    {
        $jurisdiction->resources()->detach();
        $jurisdiction->delete();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Deleted jurisdiction.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'Jurisdiction deleted successfully.');
        return response()->json('Jurisdiction deleted successfully.', 200);
    }
}
