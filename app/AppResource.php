<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppResource extends Model
{
    protected $fillable = ['app_id','resource_id'];
}
