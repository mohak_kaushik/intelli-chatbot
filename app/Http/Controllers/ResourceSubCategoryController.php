<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResourceSubCategory;
use App\ResourceCategory;
use Validator;
use Response;
use App\Activity;

class ResourceSubCategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = ResourceCategory::where('status','1')->pluck('name', 'id')->toArray();
        $query = ResourceSubCategory::query();
        if(isset($request->subcategory) && $request->subcategory!='') {
            $query->where('name', 'like', '%' . $request->subcategory . '%');
        }
        if(isset($request->category) && $request->category!='') {
            $query->where('category_id', $request->category);
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }
        $subcategories = $query->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited resource sub category list page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('resource-sub-category', compact('subcategories', 'categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'add_name' => 'required',
            'add_category' => 'required',
            'add_status' => 'required',
        ],[
            'add_name.required' => 'The name field is required.',
            'add_category.required' => 'The category field is required.',
            'add_status.required' => 'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $resourceSubCategory = new ResourceSubCategory();
        $resourceSubCategory->name = $request->add_name;
        $resourceSubCategory->category_id = $request->add_category;
        $resourceSubCategory->status = $request->add_status;
        $resourceSubCategory->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new resource sub category.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('SubCategory created successfully.', 200);
    }

    public function edit(ResourceSubCategory $resourceSubCategory)
    {
        return $resourceSubCategory;
    }

    public function update(Request $request, ResourceSubCategory $resourceSubCategory)
    {
        $validator = Validator::make($request->all(), [
            'edit_name' => 'required',
            'edit_category' => 'required',
            'edit_status' => 'required',
        ],[
            'edit_name.required' => 'The name field is required.',
            'edit_category.required' => 'The category field is required.',
            'edit_status.required' => 'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $resourceSubCategory->name = $request->edit_name;
        $resourceSubCategory->category_id = $request->edit_category;
        $resourceSubCategory->status = $request->edit_status;
        $resourceSubCategory->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated resource sub category details.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('SubCategory updated successfully.', 200);
    }
}
