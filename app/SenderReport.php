<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SenderReport extends Model
{
    public function app()
    {
        return $this->belongsTo('App\App', 'app_id', 'id');
    }
}
