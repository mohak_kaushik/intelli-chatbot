<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use App\App;
use App\ResourceAuthorityLevel;
use App\ResourceCategory;
use App\ResourceSubCategory;
use App\Jurisdiction;
use Auth;
use Validator;
use Response;
use Request as Req;
use App\Activity;

class ResourceController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:resource-list');
        $this->middleware('permission:resource-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:resource-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $apps = App::where('status','1')->pluck('name', 'id')->toArray();
        $authority_levels = ResourceAuthorityLevel::where('status','1')->pluck('name', 'id')->toArray();
        $categories = ResourceCategory::where('status','1')->pluck('name', 'id')->toArray();
        $jurisdictions = Jurisdiction::where('status','1')->pluck('name', 'id')->toArray();
        $query = Resource::query();
        if(isset($request->resource) && $request->resource!='') {
            $query->where('name', 'like', '%' . $request->resource . '%');
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }
        $resources = $query->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited resource list page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('resources', compact('resources', 'apps', 'authority_levels', 'categories', 'jurisdictions'));
    }

    public function edit(Resource $resource)
    {
        $subcategories = ResourceSubCategory::where('status','1')->where('category_id', $resource->category_id)->pluck('name', 'id')->toArray();
        return ['resource' => $resource, 'subcategories' => $subcategories, 'rapps' => $resource->apps->pluck('id'), 'rjurisdictions' => $resource->jurisdictions->pluck('id')];
    }

    public function update(Request $request, Resource $resource)
    {
        $validator = Validator::make($request->all(), [
            'edit_resource_app' => 'required',
            'edit_resource_name' => 'required',
            'edit_resource_designation' => 'required',
            'edit_resource_authority_level' => 'required',
            'edit_resource_jurisdiction' => 'required',
            'edit_resource_category' => 'required',
            'edit_resource_sub_category' => 'required',
            'edit_resource_contact_no' => 'required',
            'edit_resource_email' => 'required|email',
            'edit_resource_website' => 'nullable|url',
            'edit_resource_picture' => 'mimes:jpeg,jpg,png',
        ],[
            'edit_resource_app.required'=>'The app field is required.',
            'edit_resource_name.required'=>'The name field is required.',
            'edit_resource_designation.required'=>'The designation field is required.',
            'edit_resource_authority_level.required'=>'The authority level field is required.',
            'edit_resource_jurisdiction.required'=>'The jurisdiction field is required.',
            'edit_resource_category.required'=>'The category field is required.',
            'edit_resource_sub_category.required'=>'The sub category field is required.',
            'edit_resource_contact_no.required'=>'The contact number field is required.',
            'edit_resource_email.required'=>'The email field is required.',
            'edit_resource_email.email'=>'The email must be a valid email address.',
            'edit_resource_website.url'=>'The website format is invalid.',
            'edit_resource_picture.mimes'=>'The picture must be a file of type: jpeg, jpg, png.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        if ($request->hasFile('edit_resource_picture')) {  
            if ((($resource->picture!=NULL) || ($resource->picture!='')) && (\File::exists(public_path('upload/resource-picture/'.$resource->picture)))) {
                unlink(public_path('upload/resource-picture/'.$resource->picture));
            }
            $picture = time().'.'.request()->edit_resource_picture->getClientOriginalExtension();
            request()->edit_resource_picture->move(public_path('upload/resource-picture'), $picture);
            $resource->picture = $picture;
        }

        $resource->name = $request->edit_resource_name;
        $resource->designation = $request->edit_resource_designation;
        $resource->authority = $request->edit_resource_authority_level;
        $resource->category_id = $request->edit_resource_category;
        $resource->sub_category_id = $request->edit_resource_sub_category;
        // $resource->phrases = $request->edit_resource_phrases;
        $resource->max_unit_of_service = $request->edit_resource_max_unit_of_service;
        $resource->provision = $request->edit_resource_provision;
        $resource->refresh_time = $request->edit_resource_refresh_time;
        $resource->contact_no = $request->edit_resource_contact_no;
        $resource->address = $request->edit_resource_address;
        $resource->email = $request->edit_resource_email;
        $resource->website = $request->edit_resource_website;
        $resource->last_updated_by = Auth::user()->id;
        $resource->save();
        $resource->apps()->detach();
        $resource->jurisdictions()->detach();
        $resource->apps()->attach($request->edit_resource_app);
        $resource->jurisdictions()->attach($request->edit_resource_jurisdiction);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated resource details.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('Resource updated successfully.', 200);
    }

    public function destroy(Resource $resource)
    {
        if (($resource->picture!=NULL) && (\File::exists(public_path('upload/resource-picture/'.$resource->picture)))) {
            unlink(public_path('upload/resource-picture/'.$resource->picture));
        }
        $resource->apps()->detach();
        $resource->jurisdictions()->detach();
        $resource->delete();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Deleted resource.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'Resource deleted successfully.');
        return response()->json('Resource deleted successfully.', 200);
    }

}
