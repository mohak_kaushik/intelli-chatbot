<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('package_name');
            $table->string('operator_name');
            $table->string('operator_email');
            $table->bigInteger('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('app_categories')->onDelete('cascade');  
            $table->bigInteger('sub_category_id')->unsigned()->index();
            $table->foreign('sub_category_id')->references('id')->on('app_sub_categories')->onDelete('cascade');  
            $table->text('keywords')->nullable();
            $table->text('phrases')->nullable();
            $table->string('public_api_key', 15);
            $table->string('logo');
            $table->bigInteger('created_by')->unsigned()->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');  
            $table->bigInteger('last_updated_by')->unsigned()->index()->nullable();
            $table->foreign('last_updated_by')->references('id')->on('users')->onDelete('cascade');  
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps');
    }
}
