<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppCategory;
use App\AppSubCategory;
use App\ResourceCategory;
use App\ResourceSubCategory;
use App\ResourceAuthorityLevel;
use App\App;
use App\Chatbot;
use App\Resource;
use App\Jurisdiction;
use App\Activity;
use Auth;
use Validator;
use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('permission:app-create', ['only' => ['createNew','createNewApp']]);
        // $this->middleware('permission:chatbot-create', ['only' => ['createNew','createNewChatbot']]);
        // $this->middleware('permission:resource-create', ['only' => ['createNew','createNewResource']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $query = Activity::query();
        if(!Auth::user()->hasRole('SuperAdmin')) {
            $query->where('user_id', auth()->user()->id);
        } else {
            // $query->where('user_id', '!=', auth()->user()->id);
        }
        $activities = $query->orderBy('created_at','DESC')->paginate(5);
        return view('home', compact('activities'));
    }

    public function getCategories()
    {
        $categories = AppCategory::where('status','1')->pluck('name', 'id')->toArray();
        return $categories;
    }

    public function getSubCategories(Request $request)
    {
        $sub_categories = AppSubCategory::where('status','1')->where('category_id', $request->category_id)->pluck('name', 'id')->toArray();
        return $sub_categories;
    }

    public function getApps()
    {
        $apps = App::where('status','1')->pluck('name', 'id')->toArray();
        return $apps;
    }

    public function getResourcefieldData()
    {
        $apps = App::where('status','1')->pluck('name', 'id')->toArray();
        $authority_level = ResourceAuthorityLevel::where('status','1')->pluck('name', 'id')->toArray();
        $categories = ResourceCategory::where('status','1')->pluck('name', 'id')->toArray();
        $jurisdictions = Jurisdiction::where('status','1')->pluck('name', 'id')->toArray();
        return ['apps' => $apps, 'authority_level' => $authority_level, 'categories' => $categories, 'jurisdictions' => $jurisdictions];
    }

    public function getResourceSubCategories(Request $request)
    {
        $sub_categories = ResourceSubCategory::where('status','1')->where('category_id', $request->category_id)->pluck('name', 'id')->toArray();
        return $sub_categories;
    }

    public function createNew(Request $request)
    {
        if($request->create_type == 'App') {
            return $this->createNewApp($request->all());
        } elseif($request->create_type == 'Chatbot') {
            return $this->createNewChatbot($request->all());
        } else {
            return $this->createNewResource($request->all());
        }
    }

    private function createNewApp($data)
    {
        $validator = Validator::make($data, [
            'app_name' => 'required',
            'app_package_name' => 'required',
            'app_operator_name' => 'required',
            'app_operator_email' => 'required|email',
            // 'app_category' => 'required',
            // 'app_sub_category' => 'required',
            'logo' => 'mimes:jpeg,jpg,png',
        ],[
            'app_name.required'=>'The name field is required.',
            'app_package_name.required'=>'The package name field is required.',
            'app_operator_name.required'=>'The operator name field is required.',
            'app_operator_email.required'=>'The operator email field is required.',
            'app_operator_email.email'=>'The operator email must be a valid email address.',
            // 'app_category.required'=>'The category field is required.',
            // 'app_sub_category.required'=>'The subcategory field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $app = new App();
        if (isset($data['logo'])) {  
            if (!file_exists('upload/app-logo/')) {
                mkdir('upload/app-logo/', 0777, true);
            }
            
            $logo = time().'.'.$data['logo']->getClientOriginalExtension();
            $data['logo']->move(public_path('upload/app-logo'), $logo);
            $app->logo = $logo;
        }

        $app->name = $data['app_name'];
        $app->package_name = $data['app_package_name'];
        $app->operator_name = $data['app_operator_name'];
        $app->operator_email = $data['app_operator_email'];
        // $app->category_id = $data['app_category'];
        // $app->sub_category_id = $data['app_sub_category'];
        // $app->keywords = $data['app_keywords'];
        // $app->phrases = $data['app_phrases'];
        $app->public_api_key = $this->generateRandomString(8);
        $app->created_by = Auth::user()->id;
        $app->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new app.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('New app created successfully.', 200);
    }

    private function createNewChatbot($data)
    {
        $validator = Validator::make($data, [
            'chatbot_app' => 'required',
            'chatbot_name' => 'required',
            'chatbot_status' => 'required',
        ],[
            'chatbot_app.required'=>'The app field is required.',
            'chatbot_name.required'=>'The name field is required.',
            'chatbot_status.required'=>'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $chatbot = new Chatbot();
        $chatbot->name = $data['chatbot_name'];
        $chatbot->status = $data['chatbot_status'];
        $chatbot->created_by = Auth::user()->id;
        $chatbot->save();
        $chatbot->apps()->attach($data['chatbot_app']);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new chatbot.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('New chatbot created successfully.', 200);
    }

    private function createNewResource($data)
    {
        $validator = Validator::make($data, [
            'resource_app' => 'required',
            'resource_name' => 'required',
            'resource_designation' => 'required',
            'resource_authority_level' => 'required',
            'resource_jurisdiction' => 'required',
            'resource_category' => 'required',
            'resource_sub_category' => 'required',
            'resource_contact_no' => 'required',
            'resource_email' => 'required|email',
            // 'resource_website' => 'nullable|url',
            'logo' => 'mimes:jpeg,jpg,png',
        ],[
            'resource_app.required'=>'The app field is required.',
            'resource_name.required'=>'The name field is required.',
            'resource_designation.required'=>'The designation field is required.',
            'resource_authority_level.required'=>'The authority level field is required.',
            'resource_jurisdiction.required'=>'The jurisdiction field is required.',
            'resource_category.required'=>'The category field is required.',
            'resource_sub_category.required'=>'The sub category field is required.',
            'resource_contact_no.required'=>'The contact number field is required.',
            'resource_email.required'=>'The email field is required.',
            'resource_email.email'=>'The email must be a valid email address.',
            // 'resource_website.url'=>'The website format is invalid.',
            'logo.mimes'=>'The picture must be a file of type: jpeg, jpg, png.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $resource = new Resource();
        if (isset($data['logo'])) {  
            if (!file_exists('upload/resource-picture/')) {
                mkdir('upload/resource-picture/', 0777, true);
            }
            
            $logo = time().'.'.$data['logo']->getClientOriginalExtension();
            $data['logo']->move(public_path('upload/resource-picture'), $logo);
            $resource->picture = $logo;
        }

        $resource->name = $data['resource_name'];
        $resource->designation = $data['resource_designation'];
        $resource->authority = $data['resource_authority_level'];
        $resource->category_id = $data['resource_category'];
        $resource->sub_category_id = $data['resource_sub_category'];
        // $resource->phrases = $data['resource_phrases'];
        $resource->max_unit_of_service = $data['resource_max_unit_of_service'];
        $resource->provision = $data['resource_provision'];
        $resource->refresh_time = $data['resource_refresh_time'];
        $resource->contact_no = $data['resource_contact_no'];
        $resource->address = $data['resource_address'];
        $resource->email = $data['resource_email'];
        $resource->website = $data['resource_website'];
        $resource->created_by = Auth::user()->id;
        $resource->save();
        $resource->apps()->attach($data['resource_app']);
        $resource->jurisdictions()->attach($data['resource_jurisdiction']);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new resource.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('New resource created successfully.', 200);
    }

    public function generateRandomString($length = 10) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
