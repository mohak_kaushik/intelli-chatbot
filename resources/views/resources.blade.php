@extends('layouts.app')
@section('title', 'Resources')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Resources</h4>
        <a href="{{ route('resource-category.index') }}" class="btn titlebtn">Category</a>
        <a href="{{ route('resource-sub-category.index') }}" class="btn titlebtn2">Sub Category</a>
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="mb-4 float-left filters-input">
        <input type="text" name="search-resource" id="search-resource" placeholder="Search by name" onchange="filter()" value="{{app('request')->input('resource')}}">
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Resource</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Last Modified</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($resources as $resource)
                <tr>
                    <td>
                        <div class="thumbnail">
                            @if($resource->picture)
                            <span class="img"><img src="{{ url('upload/resource-picture') .'/'. $resource->picture }}" alt="{{ $resource->name }}"></span>
                            @else
                            <span class="img"><img src="{{ url('images/chooseprofile.png') }}" alt="{{ $resource->name }}"></span>
                            @endif
                            <p>{{ $resource->name }}</p>
                        </div>
                    </td>
                    <td>{{ $resource->user->name }}</td>
                    <td>
                        @if($resource->last_updated_by)
                            {{ \Carbon\Carbon::parse($resource->updated_at)->format('d M Y')}}
                        @else
                            {{ \Carbon\Carbon::parse($resource->created_at)->format('d M Y')}}
                        @endif
                    </td>
                    <td>
                    @if($resource->status == '1')
                        <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                    @else 
                        <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                    @endif
                    </td>
                    <td class="text-center">
                        @can('resource-edit')
                        <a href="{{ route('resources.edit',$resource->id) }}" class="resource-edit" data-toggle="modal" data-target="#editResourceData"><i class="fas fa-edit"></i></a>
                        @endcan
                        @can('resource-delete')
                        <a href="{{ route('resources.destroy',$resource->id) }}" class="resource-delete"><i class="fas fa-trash-alt"></i></a>
                        @endcan
                    </td>
                </tr>
                @empty
                    <tr><td colspan="6" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$resources->appends(['resource' => app('request')->input('resource'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editResourceData" tabindex="-1" role="dialog" aria-labelledby="editResourceData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Resource</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>['resources.update', ''],'method'=>'PUT','id'=>'edit-resource-form','files'=>true]) !!}
                <div class="formdesign">
                    <div class="row logo-field">
                        <div class="col-12">
                            <div class="chooseprofile text-center mb-3">
                                <div class="profile_pic">
                                    <img id="edit_resource_picture" src="{{url('images/chooseprofile.png')}}" alt="" />
                                </div>
                                <div class="upload-btn-wrapper">
                                    <button class=" btn btn-primary">Upload a file</button>
                                    <input type="file" name="edit_resource_picture" onchange="readURL(this, 'edit_resource_picture');" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>App <span style="color:red;">*</span></label>
                                <select name="edit_resource_app[]" class="selectpicker multipleselectbox edit_rs_app" multiple data-live-search="true">
                                @foreach($apps as $aKey => $aVal)
                                    <option value="{{ $aKey }}">{{ $aVal }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Name <span style="color:red;">*</span></label>
                                <input type="text" name="edit_resource_name" id="edit_resource_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Designation <span style="color:red;">*</span></label>
                                <input type="text" name="edit_resource_designation" id="edit_resource_designation" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Authority Level <span style="color:red;">*</span></label>
                                {!! Form::select('edit_resource_authority_level', $authority_levels, null, array('id' => 'edit_resource_authority_level', 'class' => 'form-control', 'placeholder' => 'Select Authority Level')) !!}
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Jurisdiction <span style="color:red;">*</span></label>
                                <select name="edit_resource_jurisdiction[]" class="selectpicker multipleselectbox edit_rs_jurisdiction" multiple data-live-search="true">
                                @foreach($jurisdictions as $jKey => $jVal)
                                    <option value="{{ $jKey }}">{{ $jVal }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Category <span style="color:red;">*</span></label>
                                {!! Form::select('edit_resource_category', $categories, null, array('id' => 'edit_resource_category', 'class' => 'form-control', 'placeholder' => 'Select Category')) !!}
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Subcategory <span style="color:red;">*</span></label>
                                <select name="edit_resource_sub_category" id="edit_resource_sub_category" class="form-control"><select>
                            </div>
                        </div>
                        <!-- <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Phrases</label>
                                <input type="text" name="edit_resource_phrases" id="edit_resource_phrases" class="form-control">
                            </div>
                        </div> -->
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Max Unit of Services</label>
                                <input type="text" name="edit_resource_max_unit_of_service" id="edit_resource_max_unit_of_service" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Resource Provision</label>
                                <input type="text" name="edit_resource_provision" id="edit_resource_provision" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Refresh Time</label>
                                <select name="edit_resource_refresh_time" id="edit_resource_refresh_time" class="form-control">
                                    <option value="">Select Refresh Time</option>
                                @foreach(Config::get('intelli.resource_refresh_time') as $rtKey => $rtVal)
                                    <option value="{{ $rtKey }}" @if($rtKey=='1') {{ 'selected' }} @endif>{{ $rtVal }}</option>
                                @endforeach
                                <select>
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Contact Number <span style="color:red;">*</span></label>
                                <input type="text" name="edit_resource_contact_no" id="edit_resource_contact_no" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Address </label>
                                <input type="text" name="edit_resource_address" id="edit_resource_address" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Email <span style="color:red;">*</span></label>
                                <input type="email" name="edit_resource_email" id="edit_resource_email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="edit_resource_website" id="edit_resource_website" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="footerbutton text-center p-3 w-100">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $("#edit_resource_category").on('change',function() {
            $.ajax({
                url: '{{ route('get-resource-sub-categories') }}',
                type: 'POST',
                data: {
                    'category_id': $(this).val(),
                    '_token': "{{csrf_token()}}"
                },
                success: function (response) {
                    $('select[name="edit_resource_sub_category"]').empty();
                    $('select[name="edit_resource_sub_category"]').append('<option value="">Select Subcategory</option>');
                    $.each(response, function(key, value) {
                        $('select[name="edit_resource_sub_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                },
            });
        });

        var rid = null;
        $('.resource-edit').on('click',function(e){
            e.preventDefault();
            $('.error').remove();
            let url = $(this).attr("href");
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    rid = response.resource.id;
                    $('select[name="edit_resource_sub_category"]').empty();
                    $('select[name="edit_resource_sub_category"]').append('<option value="">Select Subcategory</option>');
                    $.each(response.subcategories, function(key, value) {
                        $('select[name="edit_resource_sub_category"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                    if(response.resource.picture) {
                        $('#edit_resource_picture').attr('src', APP_URL+'/upload/resource-picture/'+response.resource.picture)
                    } else {
                        $('#edit_resource_picture').attr('src', APP_URL+'/images/chooseprofile.png')
                    }
                    $('select[name="edit_resource_app[]"]').val(response.rapps);
                    $('#edit_resource_name').val(response.resource.name)
                    $('#edit_resource_designation').val(response.resource.designation)
                    $('#edit_resource_authority_level').val(response.resource.authority)
                    $('select[name="edit_resource_jurisdiction[]"]').val(response.rjurisdictions)
                    $('#edit_resource_category').val(response.resource.category_id)
                    $('#edit_resource_sub_category').val(response.resource.sub_category_id)
                    // $('#edit_resource_phrases').val(response.resource.phrases)
                    $('#edit_resource_max_unit_of_service').val(response.resource.max_unit_of_service)
                    $('#edit_resource_provision').val(response.resource.provision)
                    $('#edit_resource_refresh_time').val(response.resource.refresh_time)
                    $('#edit_resource_contact_no').val(response.resource.contact_no)
                    $('#edit_resource_address').val(response.resource.address)
                    $('#edit_resource_email').val(response.resource.email)
                    $('#edit_resource_website').val(response.resource.website)
                    $('.selectpicker').selectpicker('refresh')
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        });

        $('#edit-resource-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action')+'/'+rid;
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        if(key=='edit_resource_app') {
                            $(".edit_rs_app:first").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        } else if(key=='edit_resource_jurisdiction') {
                            $(".edit_rs_jurisdiction:first").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        } else {
                            $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                        }
                    })
                },
            });
        });

        $('.resource-delete').on('click', function (e) {
            var url = $(this).attr("href");
            var result = confirm('Are you sure you want to delete the resource?'); 
            if (result == true) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        '_method': 'delete',
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        window.location.reload();
                    },
                });
            }
            e.preventDefault()
        });
    });

    function filter() {
        var resce = $('#search-resource').val();
        var status = $('#search-status').val();
        var url = APP_URL+"/{{Request::segment(1)}}?resource="+resce+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection