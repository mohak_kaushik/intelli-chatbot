<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\App;
use App\Resource;
use App\ResourceAuthorityLevel;
use App\ResourceCategory;
use App\ResourceSubCategory;
use App\Jurisdiction;
use App\ChatbotRequestHistory;
use App\SenderReport;
use Auth;
use Validator;
use Response;
use DB;

class ApiController extends Controller
{
    
    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'token'=>'required', 'resource_category' => 'required', 'resource_sub_category' => 'required', 'lat' => 'required', 'lng' => 'required'
        ]);
        if($validation->fails()) {
            $this->return['status'] = false;
            $this->return['error'] = $validation->messages();
        } else {
            if($request->token == 'ic1234') {
                $lat = floatval($request->lat);
                $lng = floatval($request->lng);
                $jd = [];
                $categories = ResourceCategory::where('status','1')->pluck('id', 'name')->toArray();
                $subCategories = ResourceSubCategory::where('category_id',$categories[$request->resource_category])->where('status','1')->pluck('id', 'name')->toArray();
                // $jurisdiction = Jurisdiction::where('min_lat', '<', 53.9333)->where('max_lat', '>', 53.9333)->where('min_lng', '<', -116.5765)->where('max_lng', '>', -116.5765)
                // ->toSql();
                // ->pluck('id')->toArray();
                $jurisdiction = DB::select( DB::raw("SELECT * FROM jurisdictions WHERE min_lat < $lat and max_lat > $lat and min_lng < $lng and max_lng > $lng") );
                foreach($jurisdiction as $j) {
                    array_push($jd, $j->id);
                }

                $resources = Resource::whereHas('jurisdictions', function($q) use($jd) {
                                            $q->whereIn('jurisdiction_id', $jd);
                                        })->where('category_id',$categories[$request->resource_category]??0)
                                        ->where('sub_category_id',$subCategories[$request->resource_sub_category]??0)
                                        ->get();
                if($resources->count()>0) {
                    $this->return['status'] = true;
                    $this->return['data'] = $resources;
                } else {
                    $this->return['status'] = true;
                    $this->return['data'] = $resources;
                    $this->return['message'] = 'No data found';
                }
            } else {
                $this->return['status'] = false;
                $this->return['error'] = "Authrization failed.";
            }
        }
    	return response()->json($this->return);
    }

    public function chatbotMessage(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'token'=>'required', 
            'sender_id' => 'required', 
            'sender_lat' => 'required', 
            'sender_lng' => 'required',
            'bot_category' => 'required',
            'bot_sub_category' => 'required',
            'request_message' => 'required',
            'bot_response' => 'required',
        ]);
        if($validation->fails()) {
            $this->return['status'] = false;
            $this->return['error'] = $validation->messages();
        } else {
            if($request->token == 'ic1234') {
                $geolocation = $request->sender_lat.','.$request->sender_lng;
                $georequest = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false&key=AIzaSyAI2X826A0AJ5dGzXkeQQDRkzJV5-5JHE4'; 
                $file_contents = file_get_contents($georequest);
                $json_decode = json_decode($file_contents);
                if(isset($json_decode->results[0])) {
                    $response = array();
                    foreach($json_decode->results[0]->address_components as $addressComponet) {
                        if(in_array('country', $addressComponet->types)) {
                                $response[] = $addressComponet->long_name; 
                        }
                    }

                    if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }

                    if ( $first != 'null' ) {
                        $country = $first;
                    } else {
                        $country = null;
                    }
                }
                $chatbotRequestHistory = new ChatbotRequestHistory();
                $chatbotRequestHistory->sender_id = $request->sender_id;
                $chatbotRequestHistory->sender_lat = $request->sender_lat;
                $chatbotRequestHistory->sender_lng = $request->sender_lng;
                $chatbotRequestHistory->sender_country = $country;
                $chatbotRequestHistory->bot_category = $request->bot_category;
                $chatbotRequestHistory->bot_sub_category = $request->bot_sub_category;
                $chatbotRequestHistory->request_message = $request->request_message;
                $chatbotRequestHistory->bot_response = $request->bot_response;
                $chatbotRequestHistory->save();

                $this->return['status'] = true;
                $this->return['message'] = 'Chatbot request message saved successfully.';
            } else {
                $this->return['status'] = false;
                $this->return['error'] = "Authrization failed.";
            }
        }
        return response()->json($this->return);
    }

    public function saveReport(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'token'=>'required', 
            'sender_id' => 'required', 
            'info_type' => 'required', 
            'info_report' => 'required_if:info_type,==,text',
            'info_image' => 'required_if:info_type,==,image',
            'category' => 'required',
            'sub_category' => 'required',
            'app_id' => 'required',
        ]);
        if($validation->fails()) {
            $this->return['status'] = false;
            $this->return['error'] = $validation->messages();
        } else {
            if($request->token == 'ic1234') {
                $senderReport = new SenderReport();
                $senderReport->sender_id = $request->sender_id;
                $senderReport->info_type = $request->info_type;
                if($senderReport->info_type == 'text') {
                    $senderReport->info_report = $request->info_report;
                }
                if($senderReport->info_type == 'image') {
                    $fname = time().'.' . explode('/', explode(':', substr($request->info_image, 0, strpos($request->info_image, ';')))[1])[1];
                    \Image::make($request->info_image)->save(public_path('upload/sender-report/').$fname);
                    $senderReport->info_image = $fname;
                }
                $senderReport->category = $request->category;
                $senderReport->sub_category = $request->sub_category;
                $senderReport->app_id = $request->app_id;
                $senderReport->save();

                $this->return['status'] = true;
                $this->return['message'] = 'Report saved successfully.';
            } else {
                $this->return['status'] = false;
                $this->return['error'] = "Authrization failed.";
            }
        }
        return response()->json($this->return);
    }
}
