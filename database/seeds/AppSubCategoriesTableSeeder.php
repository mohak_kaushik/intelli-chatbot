<?php

use Illuminate\Database\Seeder;
use App\AppSubCategory;

class AppSubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subCategories = [
            ['name' => 'App Sub Category 1', 'category_id' => 1],
            ['name' => 'App Sub Category 2', 'category_id' => 1],
            ['name' => 'App Sub Category 3', 'category_id' => 2],
            ['name' => 'App Sub Category 4', 'category_id' => 2],
            ['name' => 'App Sub Category 5', 'category_id' => 3],
            ['name' => 'App Sub Category 6', 'category_id' => 3],
        ];
 
        foreach ($subCategories as $sc) {
            AppSubCategory::create(['name' => $sc['name'], 'category_id' => $sc['category_id']]);
        }
    }
}
