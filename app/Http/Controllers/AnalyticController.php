<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatbotRequestHistory;
use DB;
use Carbon\Carbon;
use App\Activity;

class AnalyticController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:analytics-list', ['only' => ['index']]);
    }

    public function index(Request $request)
    {
        $countries = config('intelli.countries');
        $color = config('intelli.color');

        $analyticsDataQuery = ChatbotRequestHistory::select('sender_country','bot_category','bot_sub_category', DB::raw('count(*) as total'));
        if(isset($request->date) && ($request->date!='')) {
            $date_range = explode(' - ',$request->date);
            $sdate = Carbon::createFromFormat('d/m/Y', $date_range[0])->format('Y-m-d');
            $startDate = Carbon::createFromFormat('d/m/Y', $date_range[0])->format('d/m/Y');
            $edate = Carbon::createFromFormat('d/m/Y', $date_range[1])->format('Y-m-d');
            $endDate = Carbon::createFromFormat('d/m/Y', $date_range[1])->format('d/m/Y');
            $analyticsDataQuery->where('created_at', '>=', $sdate." 00:00:00")->where('created_at', '<=',  $edate." 23:59:59");
        } else {
            $sdate = Carbon::now()->subDays(30)->format('Y-m-d');
            $startDate = Carbon::now()->subDays(30)->format('d/m/Y');
            $edate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->format('d/m/Y');
            $analyticsDataQuery->where('created_at', '>=', $sdate." 00:00:00")->where('created_at', '<=',  $edate." 23:59:59");
        }
        $analyticsData = $analyticsDataQuery->whereNotNull('sender_country')->groupBy('sender_country','bot_category','bot_sub_category')->paginate(10);

        $countryWiseData = ChatbotRequestHistory::select('sender_country', DB::raw('count(*) as total'))
                            ->where('created_at', '>=', $sdate." 00:00:00")->where('created_at', '<=',  $edate." 23:59:59")
                            ->whereNotNull('sender_country')
                            ->groupBy('sender_country')
                            ->orderBy('total','DESC')
                            ->get();

        $totalMsg = $countryWiseData->sum('total');
        $analyticsGraphData = [];
        $analyticsMapData = [];
        foreach($countryWiseData as $ckey => $cdata) {
            if($ckey==4)
                break;
            $k = $ckey + 1;
            $count = (($cdata->total/$totalMsg)*100);
            $count = number_format((float)$count, 2, '.', '');
            if(array_key_exists($cdata->sender_country, $countries)) {
                $analyticsMapData[$countries[$cdata->sender_country]] = ['fillKey' => 'cntry_'.$k, 'count' => $count];
            }
            $analyticsGraphData[] = ['country' => $cdata->sender_country, 'count' => $count, 'color' => $color[$ckey]];
        }
        
        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited analytics page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('analytic', compact('analyticsData', 'analyticsGraphData', 'analyticsMapData', 'startDate', 'endDate'));
    }
}
