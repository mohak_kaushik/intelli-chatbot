<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJurisdictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurisdictions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('min_lat');
            $table->string('min_lng');
            $table->string('max_lat');
            $table->string('max_lng');
            $table->string('point1_lat');
            $table->string('point1_lng');
            $table->string('point2_lat');
            $table->string('point2_lng');
            $table->string('point3_lat');
            $table->string('point3_lng');
            $table->string('point4_lat');
            $table->string('point4_lng');
            $table->bigInteger('created_by')->unsigned()->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');  
            $table->bigInteger('last_updated_by')->unsigned()->index()->nullable();
            $table->foreign('last_updated_by')->references('id')->on('users')->onDelete('cascade');  
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurisdictions');
    }
}
