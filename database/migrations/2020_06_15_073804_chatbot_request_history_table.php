<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChatbotRequestHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatbot_request_history', function (Blueprint $table) {
            $table->id();
            $table->string('sender_id');
            $table->string('sender_lat');
            $table->string('sender_lng');
            $table->string('bot_category');
            $table->string('bot_sub_category');
            $table->text('request_message');
            $table->text('bot_response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatbot_request_history');
    }
}
