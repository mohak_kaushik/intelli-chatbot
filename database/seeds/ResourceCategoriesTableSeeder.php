<?php

use Illuminate\Database\Seeder;
use App\ResourceCategory;

class ResourceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Resource Category 1',
            'Resource Category 2',
            'Resource Category 3'
        ];
 
        foreach ($categories as $category) {
            ResourceCategory::create(['name' => $category]);
        }
    }
}
