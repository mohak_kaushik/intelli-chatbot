@extends('layouts.app')
@section('title', 'Chatbots')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Chatbots</h4>
        <!-- <button type="button"><i class="fas fa-plus"></i> New Chatbot </button> -->
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="app_chatbot_resouces row">
        <div class="col columnstyle app">
        <a href="http://18.222.205.248:5002/login?username=me&password=K0EHyawJbh46" target="_blank">
            <div class="card" style="background: #c6aade;">
                <h4>Health Bot</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle chatbot">
        <a href="http://13.58.114.5:5002/login?username=me&password=xNxVwU1pcPdo" target="_blank">
            <div class="card" style="background: #57a8ff;">
                <h4>Crime Bot</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle resouces">
        <a href="http://3.134.253.106:5002/login?username=me&password=Od79UK7XardA" target="_blank">
            <div class="card" style="background: #ff9a9a;">
                <h4>City Bot</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle resouces">
        <a href="http://52.14.132.240:5002/login?username=me&password=CRp05czYz8qR" target="_blank">
            <div class="card" style="background: #ff9a9a;">
                <h4>School</h4>
            </div>
        </a>
        </div>
    </div>

    <br/>
    <div class="app_chatbot_resouces row">
        <div class="col columnstyle app">
        <a href="http://3.135.236.14:5002/login?username=me&password=cLQaHKy8n7uy" target="_blank">
            <div class="card" style="background: #c6aade;">
                <h4>News</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle chatbot">
        <a href="http://3.135.223.143:5002/login?username=me&password=axFkiRRqW5b9" target="_blank">
            <div class="card" style="background: #57a8ff;">
                <h4>Products</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle resouces">
        <a href="http://3.133.122.145:5002/login?username=me&password=k0pIwllCHVKW" target="_blank">
            <div class="card" style="background: #ff9a9a;">
                <h4>General</h4>
            </div>
        </a>
        </div>
    </div>
    
    <br/>
    <div class="mb-4 float-left filters-input">
        <input type="text" name="search-chatbot" id="search-chatbot" placeholder="Search by name" onchange="filter()" value="{{app('request')->input('chatbot')}}">
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.chatbot_status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Chatbot Name</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Last Modified</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($chatbots as $chatbot)
                <tr>
                    <td>{{ $chatbot->name }}</td>
                    <td>{{ $chatbot->user->name }}</td>
                    <td>
                        @if($chatbot->last_updated_by)
                            {{ \Carbon\Carbon::parse($chatbot->updated_at)->format('d M Y')}}
                        @else
                            {{ \Carbon\Carbon::parse($chatbot->created_at)->format('d M Y')}}
                        @endif
                    </td>
                    <td>
                    @if($chatbot->status == '1')
                        <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                    @elseif($chatbot->status == '2')
                        <sapn class="status" style="background:#1d1d1b;"></sapn> {{ 'Testing' }} 
                    @else 
                        <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                    @endif
                    </td>
                    <td class="text-center">
                        @can('chatbot-edit')
                        <a href="{{ route('chatbots.edit',$chatbot->id) }}" class="chatbot-edit" data-toggle="modal" data-target="#editChatbotData"><i class="fas fa-edit"></i></a>
                        @endcan
                        @can('chatbot-delete')
                        <a href="{{ route('chatbots.destroy',$chatbot->id) }}" class="chatbot-delete"><i class="fas fa-trash-alt"></i></a>
                        @endcan
                    </td>
                </tr>
                @empty
                    <tr><td colspan="6" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$chatbots->appends(['chatbot' => app('request')->input('chatbot'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editChatbotData" tabindex="-1" role="dialog" aria-labelledby="editChatbotData" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Chatbot</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>['chatbots.update', ''],'method'=>'PUT','id'=>'edit-chatbot-form','files'=>true]) !!}
                <div class="formdesign">
                    <div class="row">
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>App <span style="color:red;">*</span></label>
                                <select name="edit_chatbot_app[]" class="selectpicker multipleselectbox edit_cb_app" multiple data-live-search="true">
                                @foreach($apps as $aKey => $aVal)
                                    <option value="{{ $aKey }}">{{ $aVal }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Name <span style="color:red;">*</span></label>
                                <input type="text" name="edit_chatbot_name" id="edit_chatbot_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 cols float-left">
                            <div class="form-group">
                                <label>Status <span style="color:red;">*</span></label>
                                <select name="edit_chatbot_status" id="edit_chatbot_status" class="form-control">
                                @foreach(Config::get('intelli.chatbot_status') as $csKey => $csVal)
                                    <option value="{{ $csKey }}" @if($csKey=='1') {{ 'selected' }} @endif>{{ $csVal }}</option>
                                @endforeach
                                <select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="footerbutton text-center p-3 w-100">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {

        var cid = null;
        $('.chatbot-edit').on('click',function(e){
            e.preventDefault();
            let url = $(this).attr("href");
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    cid = response.chatbot.id;
                    $('select[name="edit_chatbot_app[]"]').val(response.apps);
                    $('.selectpicker').selectpicker('refresh')
                    $('#edit_chatbot_name').val(response.chatbot.name)
                    $('#edit_chatbot_status').val(response.chatbot.status)
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        });

        $('#edit-chatbot-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action')+'/'+cid;
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        if(key=='edit_chatbot_app') {
                            $(".edit_cb_app:first").after("<small class='error form-text text-danger'>" + value[0] + "</small>")
                        } else {
                            $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                        }
                    })
                },
            });
        });

        $('.chatbot-delete').on('click', function (e) {
            var url = $(this).attr("href");
            var result = confirm('Are you sure you want to delete the chatbot?'); 
            if (result == true) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        '_method': 'delete',
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        window.location.reload();
                    },
                });
            }
            e.preventDefault()
        });
    });

    function filter() {
        var cht = $('#search-chatbot').val();
        var status = $('#search-status').val();
        var url = APP_URL+"/{{Request::segment(1)}}?chatbot="+cht+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection