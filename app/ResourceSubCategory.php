<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceSubCategory extends Model
{
    protected $fillable = ['name','category_id','status'];

    public function category()
    {
        return $this->belongsTo('App\ResourceCategory', 'category_id', 'id');
    }
}
