<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('designation')->nullable();
            $table->string('picture')->nullable();
            $table->bigInteger('authority')->unsigned()->index()->nullable();
            $table->foreign('authority')->references('id')->on('resource_authority_levels')->onDelete('cascade');
            $table->bigInteger('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('resource_categories')->onDelete('cascade');  
            $table->bigInteger('sub_category_id')->unsigned()->index();
            $table->foreign('sub_category_id')->references('id')->on('resource_sub_categories')->onDelete('cascade');
            $table->text('phrases')->nullable();
            $table->integer('max_unit_of_service')->nullable();
            $table->integer('current_unit')->nullable();
            $table->string('provision')->nullable();
            $table->string('refresh_time')->nullable();
            $table->string('contact_no')->nullable();
            $table->text('address')->nullable();
            $table->string('email')->nullable();
            $table->bigInteger('created_by')->unsigned()->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');  
            $table->bigInteger('last_updated_by')->unsigned()->index()->nullable();
            $table->foreign('last_updated_by')->references('id')->on('users')->onDelete('cascade');  
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
