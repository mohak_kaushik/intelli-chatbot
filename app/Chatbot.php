<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatbot extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
    
    public function apps()
    {
        return $this->belongsToMany('App\App', 'app_chatbot', 'chatbot_id', 'app_id');
    }
}
