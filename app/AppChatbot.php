<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppChatbot extends Model
{
    protected $fillable = ['app_id','chatbot_id'];
}
