@extends('layouts.app')
@section('title', 'Dashboard')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Quick Access</h4>
    </div>

    <div class="app_chatbot_resouces row">
        <div class="col columnstyle app">
        <a href="{{ route('apps.index') }}">
            <div class="card" style="background: #c6aade;"><i class="fas fa-tablet-alt"></i>
                <h4>Apps</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle chatbot">
        <a href="{{ route('chatbots.index') }}">
            <div class="card" style="background: #57a8ff;"><i class="fas fa-comments"></i>
                <h4>Chatbots</h4>
            </div>
        </a>
        </div>
        <div class="col columnstyle resouces">
        <a href="{{ route('resources.index') }}">
            <div class="card" style="background: #ff9a9a;"><i class="fas fa-users"></i>
                <h4>Resources</h4>
            </div>
        </a>
        </div>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    @role('SuperAdmin')
                    <th scope="col">User</th>
                    @endrole
                    <th scope="col">Activity</th>
                    <th scope="col">DateTime</th>
                </tr>
            </thead>
            <tbody>
            @forelse($activities as $activity)
                <tr>
                    @role('SuperAdmin')
                    <td>
                        <div class="thumbnail">
                            @if($activity->user->picture)
                                <span class="img"><img src="{{ url('upload/user-picture') .'/'. $activity->user->picture }}" alt="{{ $activity->user->name }}"></span>
                            @else
                                <span class="img"><img src="{{ url('images/chooseprofile.png') }}" alt="{{ $activity->user->name }}"></span>
                            @endif
                            <p>{{ $activity->user->name }}</p>
                        </div>
                    </td>
                    @endrole
                    <td>{{ $activity->activity }}</td>
                    <td>{{ \Carbon\Carbon::parse($activity->created_at)->format('d M Y h:i A')}}</td>
                </tr>
            @empty
                <tr><td colspan="3" align="center">No data found</td></tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$activities->links()}}
    </div>
</div>
@endsection
