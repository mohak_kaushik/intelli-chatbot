<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SenderReport;
use Carbon\Carbon;
use App\Activity;

class SenderReportController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:sender-report-list', ['only' => ['index']]);
    }

    public function index(Request $request)
    {
        $query = SenderReport::query();
        if(isset($request->sender) && $request->sender!='') {
            $query->where('sender_id', 'like', '%' . $request->sender . '%');
        }
        if(isset($request->category) && $request->category!='') {
            $query->where('category', 'like', '%' . $request->category . '%');
        }
        if(isset($request->subcategory) && $request->subcategory!='') {
            $query->where('sub_category', 'like', '%' . $request->subcategory . '%');
        }
        if(isset($request->date) && ($request->date!='')) {
            $date_range = explode(' - ',$request->date);
            $sdate = Carbon::createFromFormat('d/m/Y', $date_range[0])->format('Y-m-d');
            $edate = Carbon::createFromFormat('d/m/Y', $date_range[1])->format('Y-m-d');
            $query->where('created_at', '>=', $sdate." 00:00:00")->where('created_at', '<=',  $edate." 23:59:59");
        }
        $senderReports = $query->orderBy('created_at','DESC')->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited sender report page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('sender-reports', compact('senderReports'));
    }
}
