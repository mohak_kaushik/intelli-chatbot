<?php

use Illuminate\Database\Seeder;
use App\ResourceSubCategory;

class ResourceSubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subCategories = [
            ['name' => 'Resource Sub Category 1', 'category_id' => 1],
            ['name' => 'Resource Sub Category 2', 'category_id' => 1],
            ['name' => 'Resource Sub Category 3', 'category_id' => 2],
            ['name' => 'Resource Sub Category 4', 'category_id' => 2],
            ['name' => 'Resource Sub Category 5', 'category_id' => 3],
            ['name' => 'Resource Sub Category 6', 'category_id' => 3],
        ];
 
        foreach ($subCategories as $sc) {
            ResourceSubCategory::create(['name' => $sc['name'], 'category_id' => $sc['category_id']]);
        }
    }
}
