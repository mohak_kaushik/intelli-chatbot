var map;
var infoWindow;
var arr = [];
var flag = 0;
var markers = [];
var polygonline = [];

function initMap() {

    var center = new google.maps.LatLng(20.5937, 78.9629);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: center
    });

    // Add Merker on Click
    google.maps.event.addListener(map, 'click', function (event) {
        if (arr.length <= 3) {
            placeMarker(event.latLng);
        } else {

        }
    });

    // Search Box
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });




    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });



    // Configure the click listener.
    map.addListener('click', function (mapsMouseEvent) {
        if (arr.length <= 3) {
            arr.push({
                lat: mapsMouseEvent.latLng.lat(),
                lng: mapsMouseEvent.latLng.lng()
            });
            console.log('Lat-Lng', arr);
        }
        if (arr.length == 4 && flag == 0) {
            callbackmap();
            flag = 1;
        }

        // Get Min & Max latitude and longitude
        if (arr.length == 4) {
            var minlat = arr.map(function (p) { return p.lat });
            var maxlat = arr.map(function (p) { return p.lat });
            var minlng = arr.map(function (p) { return p.lng });
            var maxlng = arr.map(function (p) { return p.lng });
            var min_coords = {
                minlat: Math.min.apply(null, minlat),
                minlng: Math.min.apply(null, minlng)
            }
            var max_coords = {
                maxlat: Math.max.apply(null, maxlat),
                maxlng: Math.max.apply(null, maxlng)
            }
            $('#minlatitude').val(min_coords.minlat);
            $('#maxlatitude').val(max_coords.maxlat);
            $('#minlongitude').val(min_coords.minlng);
            $('#maxlongitude').val(max_coords.maxlng);
        }

        var count_arr = 1;
        $(arr).each(function (key, value) {
            if (count_arr < 5) {
                $('#pointlat_' + count_arr).val(value.lat);
                $('#pointlng_' + count_arr).val(value.lng);
            }
            count_arr++;
        });
    });

}


// Show Marker Lat Lng on Click
function placeMarker(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);

    var infowindow = new google.maps.InfoWindow({
        content: '<div class="infomessage"><h5>Marker Location</h5>' + marker.getPosition() + '</div>'
    });

    google.maps.event.addListener(marker, 'click', function () {
        console.log('yesssss')
        infowindow.open(map, marker);
    });
}

// All Markers get and Null
function setMapOnAll() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
}
// All Ploygon Line get and Null
function ploygonlinemap() {
    for (var i = 0; i < polygonline.length; i++) {
        polygonline[i].setMap(null);
    }
}


// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    $('.latlngpoints').find(':input').val('');
    setMapOnAll(null);
    ploygonlinemap(null);
    markers = [];
    arr = [];
    flag = 0;
}

// Construct the polygon.
function callbackmap() {
    var customshape = new google.maps.Polygon({
        paths: arr,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#FF0000',
        fillOpacity: 0.35
    });
    customshape.setMap(map);
    polygonline.push(customshape);

    infoWindow = new google.maps.InfoWindow;
}

// Add Jurisdiction Script Code Here
$('.mapbtn').on('click', function () {
    ploygonlinemap(null);
    setMapOnAll(null);
    $('.error').remove()
    $('#add-jurisdiction-form').attr('action', APP_URL + '/jurisdictions');
    $('#add-jurisdiction-form').attr('method', 'POST');
    $('#jurisdictionname').val('');
    $('.latlngpoints').each(function(key,value){
        $(this).find(':input').val('');
    });
    $('.jurisdictions_map').slideDown();
});

// Edit Jurisdiction Script Code Here
$('.jurisdiction-edit').on('click', function (e) {
    e.preventDefault();
    ploygonlinemap(null);
    setMapOnAll(null);
    $('.error').remove()
    $('.jurisdictions_map').slideDown();
    let url = $(this).attr('href');

    // Smooth Scroll
    $('html,body').animate({
        scrollTop: $("body").offset().top - 100
    },'slow');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response) {
            $('#add-jurisdiction-form').attr('action', APP_URL + '/jurisdictions/' + response.id);
            $('#add-jurisdiction-form').attr('method', 'PATCH');
            $('#jurisdictionname').val(response.name);
            $('#minlatitude').val(response.min_lat);
            $('#maxlatitude').val(response.max_lat);
            $('#minlongitude').val(response.min_lng);
            $('#maxlongitude').val(response.max_lng);
            var latlng = [
                { 'lat': parseFloat(response.point1_lat), 'lng': parseFloat(response.point1_lng) },
                { 'lat': parseFloat(response.point2_lat), 'lng': parseFloat(response.point2_lng) },
                { 'lat': parseFloat(response.point3_lat), 'lng': parseFloat(response.point3_lng) },
                { 'lat': parseFloat(response.point4_lat), 'lng': parseFloat(response.point4_lng) },
            ];
            //console.log(latlng);
            arr = latlng;
            callbackmap();
            count_arr = 1;
            for (var i = 0; i < arr.length; i++) {
                placeMarker(arr[i]);
                $('#pointlat_' + count_arr).val(arr[i].lat);
                $('#pointlng_' + count_arr).val(arr[i].lng);
                count_arr++;
            }
        },
        error: function (error) {
            console.log('Something went wrong, please try again later.')
        },
    });
});

