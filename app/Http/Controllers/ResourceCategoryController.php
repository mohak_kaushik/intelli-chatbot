<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResourceCategory;
use Validator;
use Response;
use App\Activity;

class ResourceCategoryController extends Controller
{
    public function index(Request $request)
    {
        $query = ResourceCategory::query();
        if(isset($request->category) && $request->category!='') {
            $query->where('name', 'like', '%' . $request->category . '%');
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }
        $categories = $query->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited resource category list page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('resource-category', compact('categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'add_name' => 'required',
            'add_status' => 'required',
        ],[
            'add_name.required' => 'The name field is required.',
            'add_status.required' => 'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $category = new ResourceCategory();
        $category->name = $request->add_name;
        $category->status = $request->add_status;
        $category->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Created new resource category.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('Category created successfully.', 200);
    }

    public function edit(ResourceCategory $resourceCategory)
    {
        return $resourceCategory;
    }

    public function update(Request $request, ResourceCategory $resourceCategory)
    {
        $validator = Validator::make($request->all(), [
            'edit_name' => 'required',
            'edit_status' => 'required',
        ],[
            'edit_name.required' => 'The name field is required.',
            'edit_status.required' => 'The status field is required.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $resourceCategory->name = $request->edit_name;
        $resourceCategory->status = $request->edit_status;
        $resourceCategory->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated resource category details.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('Category updated successfully.', 200);
    }
}
