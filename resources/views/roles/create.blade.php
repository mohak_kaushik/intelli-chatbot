@extends('layouts.app')
@section('title', 'Roles')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Add Role</h4>
    </div>

    <!-- <div class="breadcrumb-parent">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Role</li>
            </ol>
        </nav>
    </div> -->

    <div class="recentproject_list mt-5 mb-4">
        <div class="addrolecontent formdesign ">
            {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
            <div class="col-md-4">
                <div class="form-group">
                    <lable>Name <span style="color:red;">*</span></lable>
                    {!! Form::text('name', null, array('placeholder' => 'Role Name','class' => 'form-control','id' => "role_name", 'maxlength'=>'125' )) !!}
                    @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h3>Permission <span style="color:red;">*</span></h3>
                @error('permission')
                <small class="form-text text-danger font-weight-500">{{ $message }}</small>
                @enderror
                <ul class="permission_list mt-4">
                    @foreach($permissions as $key=>$perms)
                    <li>
                        <label class="container_checkbox">
                            <input type="checkbox" class="module" data-id="{{ str_replace(' ', '-', strtolower($key)) }}">
                            <span class="checkmark"></span>
                        </label>
                        <div class="content">{{$key}} <i class="fas fa-chevron-down"></i></div>
                        <div class="first_level_dropdown">
                            <ul class="sublevellist">
                                @foreach($perms as $perm)
                                <li>
                                    <label class="container_checkbox">
                                        {{ Form::checkbox('permission[]', $perm->id, false, array('class' => 'name permission', 'data-module' => str_replace(' ', '-', strtolower($key)))) }} {{ $perm->action }}
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>

            <div class="col-md-12 mt-4">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.permission_list li .content').on('click', function () {
            $(this).parents('li').find('.first_level_dropdown').slideToggle();
        });

        $('.module').on('click',function() {
            var module = $(this).attr("data-id");
            $('input[type="checkbox"][data-module="'+module+'"]').prop('checked', $(this).prop("checked"));
        });

        $('.permission').on('click',function() {
            var module = $(this).attr("data-module");
            if($(this).prop("checked")) {
                if($('input[type="checkbox"][data-module="'+module+'"]').length === $('input[type="checkbox"][data-module="'+module+'"]').filter(":checked").length) {
                    $('input[type="checkbox"][data-id="'+module+'"]').prop('checked', true);
                }
            } else {
                $('input[type="checkbox"][data-id="'+module+'"]').prop('checked', false);
            }
        });
    });
</script>
@endsection
