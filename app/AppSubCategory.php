<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSubCategory extends Model
{
    protected $fillable = ['name','category_id','status'];
}
