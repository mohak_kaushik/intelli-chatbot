<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'login', 301);

Auth::routes(['register' => false]);

Route::group(['middleware' => ['auth']], function() {
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('get-categories', 'HomeController@getCategories')->name('get-categories');
    Route::post('get-sub-categories', 'HomeController@getSubCategories')->name('get-sub-categories');
    Route::get('get-apps', 'HomeController@getApps')->name('get-apps');
    Route::get('get-resource-field-data', 'HomeController@getResourcefieldData')->name('get-resource-field-data');
    Route::post('get-resource-sub-categories', 'HomeController@getResourceSubCategories')->name('get-resource-sub-categories');
    Route::post('create-new', 'HomeController@createNew')->name('create-new');
    
    Route::resource('roles','RoleController')->except(['show']);
    Route::resource('users','UserController')->except(['create', 'show']);
    Route::match(['get', 'post'], 'profile/update', 'UserController@profileUpdate')->name('profile.update');
    Route::resource('apps','AppController');
    Route::resource('chatbots','ChatbotController');
    Route::resource('jurisdictions','JurisdictionController')->except(['create', 'show']);
    Route::resource('resources','ResourceController');
    Route::resource('resource-category','ResourceCategoryController');
    Route::resource('resource-sub-category','ResourceSubCategoryController');
    Route::get('chatbot-request-history', 'ChatbotController@requestHistory')->name('chatbot-request-history');
    Route::get('sender-reports', 'SenderReportController@index')->name('sender-reports');
    Route::get('analytics', 'AnalyticController@index')->name('analytics.index');
});

// Route::view('analytic', 'analytic');

// Route::view('apps', 'apps');

// Route::view('chatbots', 'chatbots');

// Route::view('resetpassword', 'auth/resetpassword');

// Route::view('newpassword', 'auth/newpassword');

// Route::view('managerole', 'managerole');

// Route::view('addrole', 'addrole');

// Route::view('jurisdiction', 'jurisdiction');
