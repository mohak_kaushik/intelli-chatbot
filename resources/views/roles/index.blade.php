@extends('layouts.app')
@section('title', 'Roles')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Roles</h4>
        @can('role-create')
        <a class="titlebtn" href="{{route('roles.create')}}"><i class="fas fa-plus"></i> Add Role </a>
        @endcan
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" width="100">Sr. No.</th>
                    <th scope="col">Role Name</th>
                    <th scope="col" class="text-center" width="200">Action</th>
                </tr>
            </thead>
            <tbody>
            @forelse($roles as $role)
                <tr>
                    <td>{{(($roles->currentpage()-1)*$roles->perPage()) + $loop->index+1 }}</td>
                    <td>{{$role->name}}</td>
                    <td class="text-center">
                        @can('role-edit')
                        <a href="{{ route('roles.edit',$role->id) }}"><i class="fas fa-edit"></i></a>
                        @endcan
                        @can('role-delete')
                        <a href="{{ route('roles.destroy',$role->id) }}" class="role-delete"><i class="fas fa-trash-alt"></i></a>
                        @endcan
                    </td>
                </tr>
            @empty
                <tr><td colspan="3" align="center">No data found</td></tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$roles->links()}}
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.role-delete').on('click', function (e) {
            var url = $(this).attr("href");
            var result = confirm('Are you sure you want to delete the role?'); 
            if (result == true) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        '_method': 'delete',
                        '_token': "{{csrf_token()}}"
                    },
                    success: function (data) {
                        window.location.reload();
                    },
                });
            }
            e.preventDefault()
        });
    });
</script>
@endsection
