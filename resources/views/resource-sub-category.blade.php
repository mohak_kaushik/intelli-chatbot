@extends('layouts.app')
@section('title', 'Resource Sub Category')
@section('style')
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Resource Sub Category</h4>
        <a class="titlebtn" href="javascript:void(0);" data-toggle="modal" data-target="#addSubCategoryModal"><i class="fas fa-plus"></i> Add SubCategory </a>
    </div>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="mb-4 float-left filters-input">
        <input type="text" name="search-sub-category" id="search-sub-category" placeholder="Search by name" onchange="filter()" value="{{app('request')->input('subcategory')}}">
        <select name="search-category" id="search-category" onchange="filter()">
            <option value="">Select category</option>
            @foreach($categories as $ctKey => $ctVal)
                <option value="{{ $ctKey }}" @if(request()->filled('category') && $ctKey==app('request')->input('category')) {{ 'selected' }} @endif>{{ $ctVal }}</option>
            @endforeach
        <select>
        <select name="search-status" id="search-status" onchange="filter()">
            <option value="">Select status</option>
            @foreach(Config::get('intelli.status') as $csKey => $csVal)
                <option value="{{ $csKey }}" @if(request()->filled('status') && $csKey==app('request')->input('status')) {{ 'selected' }} @endif>{{ $csVal }}</option>
            @endforeach
        <select>
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($subcategories as $subcategory)
                <tr>
                    <td>{{ $subcategory->name }}</td>
                    <td>{{ $subcategory->category->name }}</td>
                    <td>
                    @if($subcategory->status == '1')
                        <sapn class="status" style="background:#61d836;"></sapn> {{ 'Active' }} 
                    @else 
                        <sapn class="status" style="background:#ed220d;"></sapn> {{ 'Inactive' }} 
                    @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ route('resource-sub-category.edit',$subcategory->id) }}" class="sub-category-edit" data-toggle="modal" data-target="#editSubCategoryModal"><i class="fas fa-edit"></i></a>
                        <!-- <a href="{{ route('resource-category.destroy',$subcategory->id) }}" class="category-delete"><i class="fas fa-trash-alt"></i></a> -->
                    </td>
                </tr>
                @empty
                    <tr><td colspan="3" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{$subcategories->appends(['subcategory' => app('request')->input('subcategory'), 'category' => app('request')->input('category'), 'status' => app('request')->input('status')])->links()}}
    </div>
</div>

<!-- Add Category Modal -->
<div class="modal fade" id="addSubCategoryModal" tabindex="-1" role="dialog" aria-labelledby="addSubCategoryModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add SubCategory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>'resource-sub-category.store','method'=>'POST','id'=>'add-sub-category-form']) !!}
                    <div class="formdesign">
                        <div class="row">
                            <div class="w-100">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="add_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Category </label>
                                        <select name="add_category" class="form-control">
                                            <option value="">Select Category</option>
                                        @foreach($categories as $cKey => $cVal)
                                            <option value="{{ $cKey }}">{{ $cVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status </label>
                                        <select name="add_status" class="form-control">
                                        @foreach(Config::get('intelli.status') as $sKey => $sVal)
                                            <option value="{{ $sKey }}" @if($sKey=='1') {{ 'selected' }} @endif>{{ $sVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Edit Category Modal -->
<div class="modal fade" id="editSubCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editSubCategoryModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit SubCategory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert modal-msg" style="display:none;"></div>
                {!! Form::open(['route'=>['resource-sub-category.update', ''],'method'=>'PUT','id'=>'edit-sub-category-form']) !!}
                    <div class="formdesign">
                        <div class="row">
                            <div class="w-100">
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="edit_name" id="edit_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Category </label>
                                        <select name="edit_category" id="edit_category" class="form-control">
                                            <option value="">Select Category</option>
                                        @foreach($categories as $cKey => $cVal)
                                            <option value="{{ $cKey }}">{{ $cVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                                <div class="col-md-6 cols float-left">
                                    <div class="form-group">
                                        <label>Status </label>
                                        <select name="edit_status" id="edit_status" class="form-control">
                                        @foreach(Config::get('intelli.status') as $sKey => $sVal)
                                            <option value="{{ $sKey }}">{{ $sVal }}</option>
                                        @endforeach
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footerbutton text-center p-3 w-100">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        //Category add
        $('#add-sub-category-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action');
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                    error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        //Category edit
        var scid = null;
        $('.sub-category-edit').on('click',function(e){
            e.preventDefault();
            let url = $(this).attr("href");
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'GET',
                success: function (response) {
                    scid = response.id;
                    $('#edit_name').val(response.name)
                    $('#edit_category').val(response.category_id)
                    $('#edit_status').val(response.status)
                },
                error: function (error) {
                    console.log('Something went wrong, please try again later.')
                },
            });
        })

        //Category update
        $('#edit-sub-category-form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            let url = $(this).attr('action')+'/'+scid;
            $('.error').remove();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.modal-msg').addClass('alert-success');
                    $('.modal-msg').text(response);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    $('.modal-msg').show();
                },
                error: function (error) {
                    let errors = error.responseJSON
                    $.each(errors, function (key, value) {
                        $("input[name='" + key + "'],select[name='"+key+"']").after("<small class='error form-text text-danger'>" + value[0] + "</small>") 
                    })
                },
            });
        });

        //User delete
        // $('.user-delete').on('click', function (e) {
        //     var url = $(this).attr("href");
        //     var result = confirm('Are you sure you want to delete the user?'); 
        //     if (result == true) {
        //         $.ajax({
        //             url: url,
        //             type: 'POST',
        //             data: {
        //                 '_method': 'delete',
        //                 '_token': "{{csrf_token()}}"
        //             },
        //             success: function (data) {
        //                 window.location.reload();
        //             },
        //         });
        //     }
        //     e.preventDefault()
        // });
    });

    function filter() {
        var scat = $('#search-sub-category').val();
        var cat = $('#search-category').val();
        var status = $('#search-status').val();
        var url = APP_URL+"/{{Request::segment(1)}}?subcategory="+scat+"&category="+cat+"&status="+status;
        if (url) {
            window.location = url;
        }
    }
</script>
@endsection