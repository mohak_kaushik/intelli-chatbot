<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppChatbotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_chatbot', function (Blueprint $table) {
            $table->bigInteger('app_id')->unsigned()->index();
            $table->foreign('app_id')->references('id')->on('apps')->onDelete('cascade');  
            $table->bigInteger('chatbot_id')->unsigned()->index();
            $table->foreign('chatbot_id')->references('id')->on('chatbots')->onDelete('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_chatbot');
    }
}
