@extends('layouts.app')
@section('title', 'Sender Reports')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
<div class="maincontent">
    <div class="titlestrip mb-4">
        <h4>Sender Reports</h4>
        <!-- <button type="button"><i class="fas fa-plus"></i> New App </button> -->
    </div>

    @php
        $sdate = null; $edate = null;
        if(app('request')->input('date')) {
            $date_range = explode(' - ',app('request')->input('date'));
            $sdate = $date_range[0];
            $edate = $date_range[1];
        }
    @endphp

    <div class="mb-4 float-left filters-input">
        <input type="text" name="sender" id="search-sender" placeholder="Search by sender" onchange="filter()" value="{{app('request')->input('sender')}}">
        <input type="text" name="category" id="search-category" placeholder="Search by category" onchange="filter()" value="{{app('request')->input('category')}}">
        <input type="text" name="subcategory" id="search-subcategory" placeholder="Search by sub category" onchange="filter()" value="{{app('request')->input('subcategory')}}">
        <input type="text" name="datetime" id="search-date" placeholder="dd/mm/yyyy - dd/mm/yyyy" value="@if($sdate) {{$sdate . ' - ' . $edate}} @endif">
    </div>

    <div class="recentproject_list mt-5 mb-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Sender</th>
                    <th scope="col">Category</th>
                    <th scope="col">Sub Category</th>
                    <th scope="col">Report</th>
                    <th scope="col">App</th>
                    <th scope="col">Datetime</th>
                </tr>
            </thead>
            <tbody>
                @forelse($senderReports as $senderReport)
                <tr>
                    <td>{{ $senderReport->sender_id }}</td>
                    <td>{{ $senderReport->category }}</td>
                    <td>{{ $senderReport->sub_category }}</td>
                    <td>
                        <div class="thumbnail">
                            @if($senderReport->info_type == 'image')
                                <span class="img"><img src="{{ url('upload/sender-report') .'/'. $senderReport->info_image }}"></span>
                            @else
                                {{ $senderReport->info_report }}
                            @endif
                        </div>
                    </td>
                    <td>{{ $senderReport->app->name }}</td>
                    <td>{{ $senderReport->created_at }}</td>
                </tr>
                @empty
                    <tr><td colspan="7" align="center">No data found</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>

    <div>
        {{ $senderReports->appends(['sender' => app('request')->input('sender'), 'category' => app('request')->input('category'), 'subcategory' => app('request')->input('subcategory'), 'date' => app('request')->input('date')])->links() }}
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$(document).ready(function(){
    $('input[name="datetime"]').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: {
            format: "DD/MM/YYYY"
        },
        autoApply: true
    });

    $('input[name="datetime"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        var sender = $('#search-sender').val();
        var category = $('#search-category').val();
        var subcategory = $('#search-subcategory').val();
        var requestdate = $('#search-date').val();
        var url = APP_URL+"/{{Request::segment(1)}}?sender="+sender+"&category="+category+"&subcategory="+subcategory+"&date="+requestdate;
        if (url) {
            window.location = url;
        }
    });
});

function filter() {
    var sender = $('#search-sender').val();
    var category = $('#search-category').val();
    var subcategory = $('#search-subcategory').val();
    var requestdate = $('#search-date').val();
    var url = APP_URL+"/{{Request::segment(1)}}?sender="+sender+"&category="+category+"&subcategory="+subcategory+"&date="+requestdate;
    if (url) {
        window.location = url;
    }
}
</script>
@endsection