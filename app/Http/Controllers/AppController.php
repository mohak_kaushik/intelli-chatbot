<?php

namespace App\Http\Controllers;

use App\App;
use App\AppCategory;
use App\AppSubCategory;
use Illuminate\Http\Request;
use Validator;
use Response;
use Auth;
use Request as Req;
use App\Activity;

class AppController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:app-list');
        $this->middleware('permission:app-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:app-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $categories = AppCategory::where('status','1')->pluck('name', 'id')->toArray();
        $query = App::query();
        if(isset($request->app) && $request->app!='') {
            $query->where('name', 'like', '%' . $request->app . '%');
        }
        if(isset($request->status) && $request->status!='') {
            $query->where('status', $request->status);
        }
        $apps = $query->paginate(10);

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Visited app list page.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return view('apps', compact('apps', 'categories'));
    }

    public function edit(App $app)
    {
        // $subcategories = AppSubCategory::where('status','1')->where('category_id', $app->category_id)->pluck('name', 'id')->toArray();
        // return ['app' => $app, 'subcategories' => $subcategories];
        return ['app' => $app];
    }

    public function update(Request $request, App $app)
    {
        $validator = Validator::make($request->all(), [
            'edit_app_name' => 'required',
            'edit_app_package_name' => 'required',
            'edit_app_operator_name' => 'required',
            'edit_app_operator_email' => 'required|email',
            // 'edit_app_category' => 'required',
            // 'edit_app_sub_category' => 'required',
            'edit_app_logo' => 'mimes:jpeg,jpg,png',
        ],[
            'edit_app_name.required'=>'The name field is required.',
            'edit_app_package_name.required'=>'The package name field is required.',
            'edit_app_operator_name.required'=>'The operator name field is required.',
            'edit_app_operator_email.required'=>'The operator email field is required.',
            'edit_app_operator_email.email'=>'The operator email must be a valid email address.',
            // 'edit_app_category.required'=>'The category field is required.',
            // 'edit_app_sub_category.required'=>'The subcategory field is required.',
            'edit_app_logo.mimes'=>'The logo must be a file of type: jpeg, jpg, png.',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        if ($request->hasFile('edit_app_logo')) {  
            if (($app->logo!=NULL) && (\File::exists(public_path('upload/app-logo/'.$app->logo)))) {
                unlink(public_path('upload/app-logo/'.$app->logo));
            }
            $picture = time().'.'.request()->edit_app_logo->getClientOriginalExtension();
            request()->edit_app_logo->move(public_path('upload/app-logo'), $picture);
            $app->logo = $picture;
        }

        $app->name = $request->edit_app_name;
        $app->package_name = $request->edit_app_package_name;
        $app->operator_name = $request->edit_app_operator_name;
        $app->operator_email = $request->edit_app_operator_email;
        // $app->category_id = $request->edit_app_category;
        // $app->sub_category_id = $request->edit_app_sub_category;
        // $app->keywords = $request->edit_app_keywords;
        // $app->phrases = $request->edit_app_phrases;
        $app->last_updated_by = Auth::user()->id;
        $app->save();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Updated app details.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        return response()->json('App updated successfully.', 200);
    }

    public function destroy(App $app)
    {
        if (($app->logo!=NULL) && (\File::exists(public_path('upload/app-logo/'.$app->logo)))) {
            unlink(public_path('upload/app-logo/'.$app->logo));
        }
        $app->chatbots()->detach();
        $app->resources()->detach();
        $app->delete();

        //Save activity
        $activity = new Activity();
        $activity->activity = 'Deleted app.';
        $activity->user_id = \Auth::user()->id;
        $activity->save();

        Req::session()->flash('success', 'App deleted successfully.');
        return response()->json('App deleted successfully.', 200);
    }

}
